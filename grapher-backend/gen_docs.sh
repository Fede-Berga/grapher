#!/bin/bash

OF=../docs/src/api/simulation_api.md

echo "---" > $OF
echo layout: default >> $OF
echo "title: Simulation" >> $OF
echo "parent: API" >> $OF
echo "nav_order: 1" >> $OF
echo "---" >> $OF
printf "\n" >> $OF
pydoc-markdown -I ./simulation --render-toc >> $OF

OF=../docs/src/api/graph_api.md

echo "---" > $OF
echo layout: default >> $OF
echo "title: Graph service" >> $OF
echo "parent: API" >> $OF
echo "nav_order: 2" >> $OF
echo "---" >> $OF
printf "\n" >> $OF
pydoc-markdown -I ./dbGraphs --render-toc >> $OF