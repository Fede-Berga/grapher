import os
import requests
import json

# Algoritmhs Endopoint
BASE_URL = 'http://127.0.0.1:5002'

RES = {
    'simulation': {
        '0': ['f88d66ec-db67-4892-9246-642a2ab19408'],
        '1': [
            'c22c54d7-4ee7-44e3-b0f1-9443a66891ad',
            '975f72ca-b0c7-4200-94a6-479825e9641e'
        ]
    },
    'status': 200,
}


def test_1():
    # Right request
    x = requests.get(BASE_URL + '/simulation/algorithms')
    assert x.status_code == 200


def test_2():
    # POST is not supported
    x = requests.post(BASE_URL + '/simulation/algorithms')
    assert x.status_code == 405


def test_3():
    # Wrong URL
    x = requests.get(BASE_URL + '/simulation/algorithm')
    assert x.status_code == 404


PARAM_RIGHT = {
    "graph": {
        "options": {
            "type": "mixed",
            "multi": False,
            "allowSelfLoops": True
        },
        "attributes": {},
        "nodes": [
            {
                "key": "c22c54d7-4ee7-44e3-b0f1-9443a66891ad",
                "attributes": {
                    "x": 0.20928898683215108,
                    "y": 0.7855504651438013,
                    "size": 12
                }
            },
            {
                "key": "f88d66ec-db67-4892-9246-642a2ab19408",
                "attributes": {
                    "x": 0.20011467477951678,
                    "y": 0.44380733818455315,
                    "size": 12
                }
            },
            {
                "key": "975f72ca-b0c7-4200-94a6-479825e9641e",
                "attributes": {
                    "x": 0.610665139134901,
                    "y": 0.43807339310134435,
                    "size": 12
                }
            }
        ],
        "edges": [
            {
                "key": "geid_250_0",
                "source": "c22c54d7-4ee7-44e3-b0f1-9443a66891ad",
                "target": "f88d66ec-db67-4892-9246-642a2ab19408",
                "attributes": {
                    "size": 7
                },
                "undirected": True
            },
            {
                "key": "geid_250_1",
                "source": "f88d66ec-db67-4892-9246-642a2ab19408",
                "target": "975f72ca-b0c7-4200-94a6-479825e9641e",
                "attributes": {
                    "size": 7
                },
                "undirected": True
            }
        ]
    },
    "source": "f88d66ec-db67-4892-9246-642a2ab19408"
}


def test_4():
    # Right post request
    x = requests.post(
        BASE_URL + '/simulation/algorithms/bfs',
        json=PARAM_RIGHT,
    )
    assert x.status_code == 200
    assert x.json() == RES


PARAM_LOGICAL_ERROR = {
    "graph": {
        "options": {
            "type": "mixed",
            "multi": False,
            "allowSelfLoops": True
        },
        "attributes": {},
        "nodes": [
            {
                "key": "c22c54d7-4ee7-44e3-b0f1-9443a66891ad",
                "attributes": {
                    "x": 0.20928898683215108,
                    "y": 0.7855504651438013,
                    "size": 12
                }
            },
            {
                "key": "f88d66ec-db67-4892-9246-642a2ab19408",
                "attributes": {
                    "x": 0.20011467477951678,
                    "y": 0.44380733818455315,
                    "size": 12
                }
            },
            {
                "key": "975f72ca-b0c7-4200-94a6-479825e9641e",
                "attributes": {
                    "x": 0.610665139134901,
                    "y": 0.43807339310134435,
                    "size": 12
                }
            }
        ],
        "edges": [
            {
                "key": "geid_250_0",
                "source": "c22c54d7-4ee7-44e3-b0f1-9443a66891ad",
                "target": "f88d66ec-db67-4892-9246-642a2ab19408",
                "attributes": {
                    "size": 7
                },
                "undirected": True
            },
            {
                "key": "geid_250_1",
                "source": "f88d66ec-db67-4892-9246-642a2ab19408",
                "target": "975f72ca-b0c7-4200-94a6-479825e9641e",
                "attributes": {
                    "size": 7
                },
                "undirected": True
            }
        ]
    },
    "source": "f88d66ec-db67-4892-9246-642a2ab1940"
}


def test_5():
    # Logical Error
    x = requests.post(BASE_URL + '/simulation/algorithms/bfs',
                      json=PARAM_LOGICAL_ERROR)
    assert x.json()['status'] == 404


PARAM_SYNTATTICAL_ERROR = {
    "graph": {
        "options": {
            "type": "mixed",
            "multi": False,
            "allowSelfLoops": True
        },
        "attributes": {},
        "nodes": [
            {
                "key": "c22c54d7-4ee7-44e3-b0f1-9443a66891ad",
                "attributes": {
                    "x": 0.20928898683215108,
                    "y": 0.7855504651438013,
                    "size": 12
                }
            },
            {
                "key": "f88d66ec-db67-4892-9246-642a2ab19408",
                "attributes": {
                    "x": 0.20011467477951678,
                    "y": 0.44380733818455315,
                    "size": 12
                }
            },
            {
                "key": "975f72ca-b0c7-4200-94a6-479825e9641e",
                "attributes": {
                    "x": 0.610665139134901,
                    "y": 0.43807339310134435,
                    "size": 12
                }
            }
        ],
        "edges": [
            {
                "key": "geid_250_0",
                "source": "c22c54d7-4ee7-44e3-b0f1-9443a66891ad",
                "target": "f88d66ec-db67-4892-9246-642a2ab19408",
                "attributes": {
                    "size": 7
                },
                "undirected": True
            },
            {
                "key": "geid_250_1",
                "source": "f88d66ec-db67-4892-9246-642a2ab19408",
                "target": "975f72ca-b0c7-4200-94a6-479825e9641e",
                "attributes": {
                    "size": 7
                },
                "undirected": True
            }
        ]
    },
    "source": "f88d66ec-db67-4892-9246-642a2ab1940"  # source does not exists
}


def test_6():
    # syntattic error
    x = requests.post(BASE_URL + '/simulation/algorithms/bfs',
                      json=PARAM_SYNTATTICAL_ERROR)
    assert x.json()['status'] == 404
