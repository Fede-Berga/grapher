
def JSONValidator(data):
    from jsonschema import validate
    import json

    try:
        with open('utils/JSONSchema.json', 'r') as f:
            validate(instance = data, schema = json.load(f))
    except:
        return 'JSON Format not valid.'
    


    ## Check Options
    data['options']['type'] = 'undirected'
    data['options']['multi'] = False
    data['options']['allowSelfLoops'] = False

    
    ## Check Nodi

    nodes_keys = set()

    fields = ['key', 'attributes']
    for node in data['nodes']:

        # Nel caso in cui non ci fossero key e attributi
        
        for field in fields:
            if field not in node.keys():
                if field == 'key':
                    return f'Missing node field "key" in a node.'
                else:
                    return f'Missing node field "{field}" in node {node["key"]}.'
                    
            
        nodes_keys.add(node['key'])

        # Nel caso in cui non ci fossero le coordinate x y
        if 'x' not in node['attributes'].keys() or 'y' not in node['attributes'].keys():
            return f'Missing node coordinate in node {node["key"]}.'
        
        # Nel caso in cui la size non fosse uguale a 12
        try:
            if node['attributes']['size'] != 12:
                idx = data['nodes'].index(edge)
                data['nodes'][idx]['attributes']['size'] = 12
        except:
            idx = data['nodes'].index(edge)
            data['nodes'][idx]['attributes']['size'] = 12

    # Nel caso in cui ci fossero chiavi duplicate
    if len(nodes_keys) != len(data['nodes']):
        return 'Found non-unique node keys.'
    
    ## Check Archi

    edges_keys = set()

    fields = ['key', 'attributes', 'source', 'target']
    for edge in data['edges']:


        # Nel caso in cui non ci fossero key, attributi, source e target
        for field in fields:
            if field not in edge.keys():
                if field == 'key':
                    return f'Missing node field "key" in an edge.'
                else:
                    return f'Missing node field "{field}" in edge {edge["key"]}.'
                
        edges_keys.add(edge['key'])

        # Nel caso in cui la size non fosse uguale a 7
        try: 
            if edge['attributes']['size'] != 7:
                idx = data['edges'].index(edge)
                data['edges'][idx]['attributes']['size'] = 7
        except:
            idx = data['edges'].index(edge)
            data['edges'][idx]['attributes']['size'] = 7

        # Nel caso in cui vi fosse un cappio
        if edge['source'] == edge['target']:
            return f'Loop for edge {edge["key"]}.'

    # Nel caso in cui ci fossero chiavi duplicate
    if len(edges_keys) != len(data['edges']):
        return 'Found non-unique edge keys.'
    

    return data


    


    
        
    
