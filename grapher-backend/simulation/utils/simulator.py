from utils.algorithms import Algoritms
import networkx as nx

class Simulator:

    def __init__(self, graph):
        self.graph = nx.Graph()
        self.__buildGraph(graph)

    #Private method for building the networkx undirected graph from the graph received
    def __buildGraph(self, graph):
        #Loop over nodes
        for i in graph["nodes"]:
            self.graph.add_node(i["key"], x=i["attributes"]["x"], y=i["attributes"]["y"])
        
        #Loop over edges
        for i in graph["edges"]:
            self.graph.add_edge(i["source"], i["target"])
    
    def simulate():
        pass

class SimulatorFactory:

    def __init__(self) -> None:
        pass

    def get_simulator(self, algorithm:Algoritms, graph, source = None) -> Simulator:
        match algorithm:
            case Algoritms.BFS:
                return BFSsimulator(graph, source)
            case Algoritms.DFS:
                return DFSsimulator(graph)
            case _:
                return None

class BFSsimulator(Simulator):

    def __init__(self, graph, source_id):
        super(BFSsimulator, self).__init__(graph)
        self.source_id = source_id
    
    def simulate(self):
        return dict(enumerate(nx.bfs_layers(self.graph, [self.source_id])))

class DFSsimulator(Simulator):

    def __init__(self, graph):
        super(DFSsimulator, self).__init__(graph)
    
    def simulate(self):
        return "Simulating DFS..."