from html import escape
#import sys; print(sys.path)
from flask import Flask, jsonify, make_response, request
from flask_cors import CORS
from utils.algorithms import Algoritms
from utils.simulator import SimulatorFactory
import json

app = Flask(__name__, instance_relative_config=True)
CORS(app)

@app.route('/simulation/algorithms', methods=['GET'])
def get_algorithms():

    """
    Get the list of the available algorithms:

    * **/simulation/algorithms/ [<span style="color:red">POST</span>]** 

    `(Response) content-type = application/json`

    Returns a list of algorithms available to be simulated.
    Here an example of a JSON response:

    ```json
    {
        "status": 200, 
        "algorithms": ["BFS", "DFS"]
    }
    ```

    """

    response = {
        'status': 200, 
        'algorithms': [e._name_ for e in Algoritms],
    }

    return response


@app.route('/simulation/algorithms/<algorithm>', methods=['POST'])
def get_simulation(algorithm):

    """
    Start the simulation:

    * **/simulation/algorithms/\<algorithm\> [<span style="color:red">GET</span>]**
    
    `(Request) content-type = application/json`

    The JSON passed as parameter should have the form:

    ```json
    {
        "graph": {
            "The object containing the graph representation"
        }
        "source": "The source node"
    }
    ```

    `(Response) content-type = application/json`

    Returns a JSON containing each one of the steps (iterations) of the choosen algorithm for the provided graph.
    The JSON request must contain a _source_ field, containing the id of the starting node; a _graph_ field, containing the
    graph object.
    Whether the requests is successful, the API will return a JSON formed like the first one; Otherwise, will be returned a JSON containing an   error message, like the second one:

    ```json
    {
        "status": 200, 
        "simulation": {
            "..."
        }
    }
    ```

    ```json
    {
        "status": 400,
        "error": "The error depends on the specific JSON passed as request",
    }
    ```

    """

    content = {}

    try:
        content  = request.json
        #app.logger.info(json.dumps(content, indent=4))
    except Exception as e:
        app.logger.info("Error parsing JSON data:", e)

    # check for fields
    if not 'source' in content:
        return {
            'status': 400, 
            'error': f'The request should contain a \'source\' field',
        }
    
    if not 'graph' in content:
        return {
            'status': 400, 
            'error': f'The request should contain a \'graph\' field',
        }

    graph = content['graph']
    source = content['source']

    # Check for the algorithm 
    if algorithm.upper() not in [e._name_ for e in Algoritms]:
        app.logger.error("Bad algorithm : Check for the algorithm")
        return {
            'status': 404, 
            'error': f'no such algorithm : {escape(algorithm.upper())}',
        }

    # Should check for the validity fo the graph, but it doean't work

    #app.logger.info(json.dumps(graph, indent=4))

    #graph = JSONValidator(json.dumps(graph))

    #if (type(graph) == str):
    #    return {
    #        'status': 400, 
    #        'response': f'Invalid graph: {graph}'
    #    }

    # Check for the source node to be in the graph

    node_ids = [node['key'] for node in graph['nodes']]

    if source not in node_ids :
        app.logger.error(f"Source node \'{source}\' is not in the graph")
        return {
            'status': 404, 
            'error': f'source node \'{source}\' is not in the graph',
        }

    # Now it should simulate

    simulation = SimulatorFactory().get_simulator(Algoritms[algorithm.upper()], graph=graph, source=source).simulate()

    app.logger.info(json.dumps(simulation, indent=4))

    return {
        'status': 200, 
        'simulation': simulation,
    }
