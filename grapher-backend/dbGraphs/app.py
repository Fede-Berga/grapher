from flask import Flask, request, jsonify
from flask_cors import CORS
import json
from bson import ObjectId
from utils.DBManager import DBConnect
from utils.JSONUtils import JSONValidator

COLLECTION_NAME = 'graphs'

app = Flask(__name__)
CORS(app)


@app.route('/graph', methods=['POST'])
def upload_graph():

    """
    Upload a graph:

    * **/graph [<span style="color:red">POST</span>]**

    `(Request) content-type = application/json`

    `(Response) content-type = application/json`

    Allows you to upload a graph in JSON format to the platform.
    This JSON must have a well-formed structure, viewable on the file containing the [JSON Schema](JSONSchema.json).
    If the graph contains nodes or arcs whose value of the **size** field is different from the standard one, it is automatically modified.
    If the JSON also contains values of **options.type**, **options.multi** or **options.allowSelfLoops** other than the standard ones (i.e. "undirected", false, false), they will be corrected automatically.
    If the JSON is not well-formed, the service will return an error, specifying the missing field. On the contrary, if it is accepted, the id of the graph will be returned in the _response_ field. Here are two examples of responses (the first in case of error, the second in case of success):

    ```json
    {
        "status" : "error",
        "response" : "Missing field 'key' in a node."
    }
    ```

    ```json
    {
        "status" : "ok",
        "response": "<id>"
    }
    ```
    """

    content = request.get_json()

    content = JSONValidator(content)

    # Se il validatore ha ritornato None viene restituito un errore
    if (type(content) == str):
        response = {'status': 'error', 'response': f'Invalid graph: {content}'}
        response = jsonify(response)
        response.headers["Content-Type"] = "application/json"
        return response, 500


    # Connessione al DB Mongo
    try:
        db = DBConnect()
    except:
        response = {'status': 'error', 'response': 'Database unreachable.'}
        response = jsonify(response)
        response.headers["Content-Type"] = "application/json"
        return response, 500

    collection = db[COLLECTION_NAME]


    try:
        id = collection.insert_one(content).inserted_id
    except:
        response = {'status': 'error', 'response': 'Insert failed.'}
        response = jsonify(response)
        response.headers["Content-Type"] = "application/json"
        return response, 500


    response = {'status': 'ok', 'response': str(id)}
    response = jsonify(response)
    response.headers["Content-Type"] = "application/json"
    return response, 200



@app.route('/graph/<id>', methods=['GET'])
def get_graph(id):

    """
    Downlaod a graph:
    
    * **/graph/\<id\> [<span style="color:red">GET</span>]**

    `(Request) content-type = application/json`

    `(Response) content-type = application/json`

    Allows the return of a graph in JSON format by identifier. 
    If the requested id does not match any element saved on the Cloud, a 404 error will be returned. 
    On the contrary, the JSON will be returned via the _response_ field via string. 
    Here are two examples of responses (the first in case of error, the second in case of success):

    ```json
    {
         "status" : "error",
         "response": "Graph not found."
    }
    ```

    ```json
    {
         "status" : "ok",
         "response": "<json_graph>"
    }
    ```
    """

    # Connessione al DB Mongo
    try:
        db = DBConnect()
    except:
        response = {'status': 'error', 'response': 'Database unreachable.'}
        response = jsonify(response)
        response.headers["Content-Type"] = "application/json"
        return response, 500
    
    collection = db[COLLECTION_NAME]

    try:
        objId = ObjectId(id)
    except:
        response = {'status': 'error', 'response': 'Id not valid.'}
        response = jsonify(response)
        response.headers["Content-Type"] = "application/json"
        return response, 500     

    graph = collection.find_one({'_id':objId})
    
    # Qualora la ricerca non abbia prodotto alcun risultato
    if (not graph):
        response = {'status': 'error', 'response': 'Graph not found.'}
        response = jsonify(response)
        response.headers["Content-Type"] = "application/json"
        return response, 404
    
    del graph['_id']

    graph = json.dumps(graph)
    
    response = {'status': 'ok', 'response': graph}
    response = jsonify(response)
    response.headers["Content-Type"] = "application/json"
    return response, 200




if __name__ == '__main__':
    app.run(host="0.0.0.0", port = 5000)
