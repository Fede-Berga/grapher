# Upload/restore graph to/from cloud

In this section is explained how to run the tests for the back-end logic when uploading/restoring your graph to/from cloud.

The following features have been automatically checked:
- Verify empty json rejection
- Verify graph with a node with missing coordinate rejection
- Verify graph with a node with missing id rejection
- Verify graph with an edge with missing id rejection
- Verify graph with a self loop rejection
- Verify the upload of a correct graph
- Verify the restore of a correct graph

Follow these steps in order to run the test above, starting from the project root folder:
```sh
    cd grapher-backend/dbGraphs/test
    docker-compose up -d --build
    docker logs tester -f
    docker-compose down
```