
class TestClass:
    
    # Verifica il rifiuto di un json vuoto
    def test_empty_json(self):
        import requests
        import json
        with open('json_files/empty_json.json','r') as j:
            j = json.load(j)
            response = requests.post("http://app:5000/graph", json = j)
            assert response.json()['response'] == "Invalid graph: JSON Format not valid."
    
    # Verifica il rifiuto di un grafo con una coordinata di un nodo mancante
    def test_missing_coordinate(self):
        import requests
        import json
        with open('json_files/missing_coordinate.json','r') as j:
            j = json.load(j)
            response = requests.post("http://app:5000/graph", json = j)
            assert response.json()['response'] == "Invalid graph: JSON Format not valid."

    # Verifica il rifiuto di un grafo con un nodo senza key
    def test_missing_node_key(self):
        import requests
        import json
        with open('json_files/missing_node_key.json','r') as j:
            j = json.load(j)
            response = requests.post("http://app:5000/graph", json = j)
            assert response.json()['response'] == "Invalid graph: JSON Format not valid."

    # Verifica il rifiuto di un grafo con un arco senza key
    def test_missing_edge_key(self):
        import requests
        import json
        with open('json_files/missing_edge_key.json','r') as j:
            j = json.load(j)
            response = requests.post("http://app:5000/graph", json = j)
            assert response.json()['response'] == "Invalid graph: JSON Format not valid."

    # Verifica il rifiuto di un grafo con un self loop
    def test_self_loop(self):
        import requests
        import json
        with open('json_files/self_loop.json','r') as j:
            j = json.load(j)
            response = requests.post("http://app:5000/graph", json = j)
            assert response.json()['response'] == 'Invalid graph: Loop for edge geid_239_0.'

    
    # Verifica il corretto upload (insert) di un grafo
    def test_insert(self):
        import requests
        import json
        with open('json_files/correct.json','r') as j:
            j = json.load(j)
            response = requests.post("http://app:5000/graph", json = j)
            assert response.json()['status'] == 'ok'

    # Verifica il corretto retrieve (get) di un grafo
    def test_get(self):
        import requests
        import json
        with open('json_files/correct.json','r') as j:
            j = json.load(j)
            res = requests.post("http://app:5000/graph", json = j)
            id = res.json()['response']
            res = requests.get(f'http://app:5000/graph/{id}')
            res = res.json()['response']
            assert json.loads(res) == j

