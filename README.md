<div align="center">

<img src="grapher-frontend/src/img/logo.png" alt="Lit-LLaMA" width="128"/>

#  Grapher

![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/Fede-Berga/grapher?branch=sprint_1_branch) ![GitLab language count](https://img.shields.io/gitlab/languages/count/Fede-Berga/grapher) ![GitLab](https://img.shields.io/gitlab/license/Fede-Berga/grapher)

</div>

Grapher is an awesome tool for creating your own graph and testing some algoritms on it!

## High Level Architecture

The Grapher architecture is a three tier one:

<div align="center">
    <img src="images/high_lvl_arch.png" alt="Lit-LLaMA" width="70%"/>
</div>

- **Web Client**
    - Interface to enjoy the service.
    - Allows a user to draw, import and export graphs.
    - Visualizes the simulation of algorithms.
- **Web Server**
    - Exposes an *API* made up of two services:
        - Simulation : Allows the user to perform algorithms on the provided graph.
        - Graph : allows the user to perform **CRUD** operations on graphs.
- **Database**
    - Memorizes users, graphs and simulations.

# Installation guide

To install the Grapher app it is necessary to install [Docker](https://docs.docker.com/desktop/) on your machine.

## Follow these steps to install the Grapher web-app on your **local** machine

1. Clone the gitlab repository <br>
```sh
git clone https://gitlab.com/Fede-Berga/grapher.git
```
2. Enter the grapher folder <br>
```sh 
cd grapher/
```
3. Start the whole app <br>
```sh 
docker compose up -d
```
4. Finally head to **localhost:3000/** and enjoy! 😃
