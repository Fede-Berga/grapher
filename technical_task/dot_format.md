# DOT file format converter

Per poter supportare il formato dot sulla piattaforma come richiesto dal primo meeting è necessario implementare un parser
- da JSON a DOT
- da DOT a JSON

Non sono emersi tool che possono essere integrati immediatamente sulla piattaforma per svolgere questo compito.<br>
E' quindi necessario realizzare un parser ex-novo apposta per la piattaforma.


## Conversione da JSON a DOT
Si sfrutta il JSON generato da sigma-js e lo si va a parsare manualmente in modo da creare un file DOT.

## Conversione da DOT a JSON
Viene sfruttata una [libreria typescript](https://github.com/ts-graphviz/parser) che è da supporto per poter passare da file DOT ad oggetto in memoria, il quale verrà poi sfruttato per costruire un file JSON per poterlo importare sulla piattaforma.\
Per poter importare successivamente il file JSON su sigma, si sfrutta il metodo offerto da graphology:
```js
    graph.import(data);
```
dove *data* è il file JSON in memoria.\
Tale import dovrebbe essere compiuto nel file UndirectedGraphComponent.tsx

## Sintassi del file DOT esportato dalla piattaforma
- Il grafo deve necessariamente essere di tipo *graph* che per ora è l'unico supportato dalla piattaforma.
- Segue all'interno una sezione in cui sono elencati tutti i nodi, in cui come attributi sono presenti le coordinate x e y.
- Infine è presente una sezione in cui sono rappresentati gli archi del grafo.\
Esempio di un file prodotto dalla piattaforma:
```
graph G {
        a [x = 0.12979592051703176, y = 0.8008163403444253];
        b [x = 0.12979592051703176, y = 0.8008163403444253];
        c [x = 0.12979592051703176, y = 0.8008163403444253];
        d [x = 0.12979592051703176, y = 0.8008163403444253];
        a -- b;
        b -- c;
        c -- d;
        d -- a;
        b -- d;
    }
```
*G* è il nome del grafo generato dalla piattaforma, tale nome verrà generato come già realizzato per i nomi dei file JSON scaricabili. 

## Sintassi del file DOT importato sulla piattaforma
- Il grafo deve necessariamente essere di tipo graph che per ora è l'unico supportato dalla piattaforma.
- Può essere scelto un qualunque nome per il grafo, in quanto è irrilevante per l'importazione.
- Tutti i nodi devono essere dichiarati con le proprietà *x* e *y*.
- Negli archi devono essere presenti esclusivamente nodi dichiarati.<br>

Sarà poi necessario adattare il file DOT in JSON in base alle proprietà che necessita sigma per poter disegnare il grafo.