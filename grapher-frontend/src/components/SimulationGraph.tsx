import { useSigma } from "@react-sigma/core";
import React, { useContext, useEffect } from "react";
import GraphContext from "../storage/context";

interface SimulationProps{
    activeIteration: number;
    state: any;
    trigger : any;
    nodeOver : string
    iteractionOver : number | null
}

const SimulationGraph: React.FC<SimulationProps> = ({activeIteration, state, trigger, nodeOver, iteractionOver}) => {
    const sigma = useSigma();
    const graphCtx = useContext(GraphContext);
    
    useEffect(() => {
        if(sigma.getGraph().nodes().length === 0)
            sigma.getGraph().import(JSON.parse(graphCtx.jsonGraph))

        for(let i = 0; i < sigma.getGraph().nodes().length; i++)
            sigma.getGraph().setNodeAttribute(sigma.getGraph().nodes()[i], "color", sigma.getSetting("defaultNodeColor"));

        for (let i = 0; i < activeIteration; i++) {       
            for(let j = 0; j < state.simulation[i].length; j++)      
                sigma.getGraph().setNodeAttribute(state.simulation[i][j], "color", state.simulation[i][j] === nodeOver || (i + 1) === iteractionOver ? "orange" : "#cc0000");
        }

        sigma.resize();
        sigma.refresh();

    }, [sigma, graphCtx, state, activeIteration, trigger, nodeOver, iteractionOver]);

    return null;
};

export default SimulationGraph;