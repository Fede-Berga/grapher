import logo from '../img/logo.png'
import './NavbarCustom.css';
import { useContext } from 'react';
import GraphContext from '../storage/context';
import { Nav, Navbar, Image } from 'react-bootstrap';
import HTUGraphDesign from './HTUGraphDesign';
import DownloadButton from './DownloadButton';
import UploadButton from './UploadButton';
import UploadCloudButton from './UploadCloudButton';
import SimulationButton from './SimulationButton';
import HowToUseButtonSimulationPanel from './HowToUseSimulationPanel';
import HomeButton from './HomeButton';

function NavbarCustom({ isInWhiteboard }: { isInWhiteboard: boolean }) {
  const graphCtx = useContext(GraphContext);

  function isEmptyWhiteboard() {
    if (graphCtx.jsonGraph === '{}')
      return true;
    else {
      var jgraph = JSON.parse(graphCtx.jsonGraph);
      return jgraph.nodes.length === 0
    }

  }

  return (
    <>
      <Navbar bg="light"
        variant="light"
        className="shadow-sm p-3 bg-white"
        style={{
          height: "8vh"
        }}>
        <Navbar.Brand style={{ height: '100%', padding: '0px 15px 0px 15px' }}
          href='https://www.youtube.com/watch?v=dQw4w9WgXcQ'>
          <Image
            src={logo}
            className="App-logo"
            style={{
              width: 'auto',
              height: "100%",
              margin: '0px 15px 0px 0px'
            }}
          />
          <div style={{ paddingTop: '10px' }}>Grapher</div>
        </Navbar.Brand>
        <Navbar.Toggle />
        <Navbar.Collapse className="justify-content-end" id='navCollapse'>
          <Nav >
            {isEmptyWhiteboard() && isInWhiteboard && <UploadButton></UploadButton>}
            {!isEmptyWhiteboard() && isInWhiteboard && <DownloadButton></DownloadButton>}
            {!isEmptyWhiteboard() && isInWhiteboard && <SimulationButton></SimulationButton>}
            {!isEmptyWhiteboard() && isInWhiteboard && <UploadCloudButton></UploadCloudButton>}
            {isInWhiteboard && <HTUGraphDesign></HTUGraphDesign>}
            {!isInWhiteboard && <HomeButton></HomeButton>}
            {!isInWhiteboard && <HowToUseButtonSimulationPanel></HowToUseButtonSimulationPanel>}
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </>
  );
}

export default NavbarCustom;
