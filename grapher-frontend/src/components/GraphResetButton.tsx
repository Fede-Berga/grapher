import { useSigma } from '@react-sigma/core';
import { FC, useContext, useState } from 'react';
import { Button, Modal } from 'react-bootstrap';
import GraphContext from '../storage/context';
import "./GraphResetButton.css";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash } from '@fortawesome/free-solid-svg-icons';


const GraphResetButton: FC = () => {
    const [isModalOpen, setIsModalOpen] = useState(false);
    const graphCtx = useContext(GraphContext);
    const sigma = useSigma();
    const handleResetConfirmation = () => {

        graphCtx.updateGraph("{}");
        sigma.getGraph().clear();
        graphCtx.updateRefreshedCtx(true);

        setIsModalOpen(false); // Chiudi il modal dopo l'azione di azzeramento
    };

    return (
        <>
            <div className="ResetButton" style={{ display: "flex", justifyContent: "center", alignContent: "center" }}>
                <button style={{ padding: "0 0 2 1", background: "none", border: "none" }} onClick={() => setIsModalOpen(true)}>
                    <FontAwesomeIcon icon={faTrash} />
                </button>
            </div>
            <Modal show={isModalOpen} onHide={() => setIsModalOpen(false)}>
                <Modal.Header>
                    <Modal.Title>
                        Confirm
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    Do you really want to reset the whiteboard?
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="danger" onClick={handleResetConfirmation}>Yes</Button>
                    <Button onClick={() => setIsModalOpen(false)}>No</Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

export default GraphResetButton;