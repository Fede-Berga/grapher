import { FC, useContext, useEffect, useState } from "react";
import { Button, Form, InputGroup, Modal, Nav, OverlayTrigger, Spinner, Tooltip } from "react-bootstrap";
import axios from "axios";
import GraphContext from "../storage/context";
import { useCopyToClipboard } from 'usehooks-ts';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faClipboard } from "@fortawesome/free-regular-svg-icons";
import { faExclamationCircle } from "@fortawesome/free-solid-svg-icons";

const BASE_URL = 'http://127.0.0.1:5001';


const UploadCloudButton: FC = () => {
    const [isUploading, setUploading] = useState<boolean>(false);
    const graphCtx = useContext(GraphContext);
    const [ , copy] = useCopyToClipboard();

    const renderTooltip = (props: any) => (
        <Tooltip id="button-tooltip" {...props}>
            Copy to Clipboard
        </Tooltip>
    );



    useEffect(() => {

        if (isUploading) {

            axios.post(BASE_URL + "/graph", JSON.parse(graphCtx.jsonGraph), {
                headers: {
                    "Content-Type": "application/json"
                }
            })
                .then((response) => {
                    console.log(response.data)
                    const uploading = document.querySelector('#uploading');
                    const uploading_done = document.querySelector('#uploading_done');
                    uploading?.setAttribute("style", "display: none;");
                    uploading_done?.setAttribute("style", "display: block;");
                    (document.getElementById("graphID") as HTMLInputElement).value = window.location.href + "graph/" + response.data.response;
                })
                .catch((err) => {
                    console.log(err);
                    const uploading = document.querySelector('#uploading');
                    const uploading_error = document.querySelector('#uploading_error');
                    uploading?.setAttribute("style", "display: none;");
                    uploading_error?.setAttribute("style", "display: block;");
                });
        }

    }, [isUploading, graphCtx])

    return (
        <>
            <Nav.Link onClick={() => setUploading(true)}>
                <dfn data-info="Upload to Cloud">
                    <span className="icon">
                        <i style={{ color: '#477cff' }} className="fa-solid fa-cloud-arrow-up"></i>
                    </span>
                </dfn>
            </Nav.Link>

            <Modal
                show={isUploading}
                onHide={() => setUploading(false)}
                backdrop="static"
                keyboard={false}
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >

                <Modal.Body id="uploading" style={{ textAlign: "center" }}>
                    <h2>Uploading...</h2><br></br>
                    <Spinner animation="grow" variant="primary" />
                </Modal.Body>

                <Modal.Body id="uploading_done" style={{ display: "none" }}>
                    <div style={{ textAlign: "center" }}>
                        <h2>Upload Successful &nbsp;<i style={{ color: '#3b9a11' }} className="fa-regular fa-circle-check fa-lg"></i></h2><br></br>

                        <InputGroup className="mb-3">
                            <Form.Control readOnly type="text" id="graphID" />
                            <OverlayTrigger
                                placement="top"
                                delay={{ show: 250, hide: 400 }}
                                overlay={renderTooltip}>

                                <Button variant="outline-primary" onClick={() => copy((document.getElementById("graphID") as HTMLInputElement).value)}>
                                    <FontAwesomeIcon icon={faClipboard} />
                                </Button>
                            </OverlayTrigger>
                        </InputGroup>
                        <Button variant="success" onClick={() => setUploading(false)}>Done</Button>
                    </div>
                </Modal.Body>

                <Modal.Body id="uploading_error" style={{ display: "none" }}>
                    <div style={{ textAlign: "center" }}>
                        <h2>Error while uploading &nbsp;<FontAwesomeIcon icon={faExclamationCircle} size="lg" style={{color: "#ff0000"}} /></h2><br></br>
                            
                        <Button variant="danger" onClick={() => setUploading(false)}>Exit</Button>
                    </div>
                </Modal.Body>


            </Modal>
        </>);

}

export default UploadCloudButton;