import { FC, useEffect, useContext, useState } from "react";
import { Alert, Button, Nav, Modal, Form } from "react-bootstrap";
import GraphContext from "../storage/context";
import { useNavigate } from 'react-router-dom';
import axios from 'axios';

const BASE_URL = 'http://127.0.0.1:5002/simulation';
const DEFAULT_ALGORITHM = '-- Select algorithm --';
const DEFAULT_SOURCE_NODE = '-- Select source node --';

const SimulationButton: FC = () => {

    const [showModal, setShowModal] = useState<boolean>(false);
    const [selectedAlgorithm, setSelectedAlgorithm] = useState<string | null>();
    const [selectedSource, setSelectedSource] = useState<string | null>();
    const [algorithms, setAlgorithms] = useState<string[]>([]);
    const [isSimulating, setSimulating] = useState<boolean>(false);
    const [showAlert, setShowAlert] = useState<boolean>(false);
    const graphCtx = useContext(GraphContext);
    const navigate = useNavigate();

    const getSourceNodes = () => {
        var nodes = JSON.parse(graphCtx.jsonGraph).nodes;

        if (!nodes) {
            console.log('graph context is empty')
            return null
        }

        //console.log('nodes : ', nodes)

        return nodes.map((node: any) => {
            //console.log(node)
            return node.key;
        })
    }

    const checkForValidInput = () => {
        return !isSimulating && selectedAlgorithm && selectedSource;
    }

    const handleClose = () => {
        setSelectedAlgorithm(null);
        setSelectedSource(null);
        setShowModal(false);
        setSimulating(false)
    }

    const handleOpen = () => {
        setSelectedAlgorithm(null);
        setSelectedSource(null);
        setShowModal(true);
        setSimulating(false)
    }

    useEffect(() => {

        if (algorithms.length === 0) {
            axios.get("/algorithms", {
                baseURL: BASE_URL,
            })
                .then((response) => {
                    setAlgorithms(response.data.algorithms)
                })
                .catch((err) => {
                    console.log(err);
                });
        }

        if (isSimulating) {
            const data = JSON.stringify({
                'graph': JSON.parse(graphCtx.jsonGraph),
                'source': selectedSource
            });
            axios.post(BASE_URL + "/algorithms/" + selectedAlgorithm, data, {
                headers : {
                    "Content-Type": "application/json"
                }
            })
            .then((response) => {
                setSimulating(false)
                navigate('/simulation', {state : response.data})
            })
            .catch((err) => {
                console.log(err);
                setSimulating(false)
            });
        }
    }, [isSimulating, graphCtx, navigate, selectedAlgorithm, selectedSource, algorithms.length])

    return (
        <>
            <Nav.Link onClick={handleOpen}>
                <dfn data-info="Simulate">
                    <span className="icon">
                        <i style={{ color: '#477cff' }} className="fa-regular fa-circle-play"></i>
                    </span>
                </dfn>
            </Nav.Link>

            <Modal show={showModal} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Chose the parameter for your simulation</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <Form>
                        <Form.Group>
                            <Form.Label>Algorithm</Form.Label>
                            <Form.Select onChange={(e) => {
                                setSelectedAlgorithm(e.target.value);
                                console.log('onChange Algo')
                            }}>
                                <option disabled selected>{DEFAULT_ALGORITHM}</option>
                                {
                                    algorithms.map((algorithm) => {
                                        return (
                                            <option key={algorithm} value={algorithm}>
                                                {algorithm}
                                            </option>
                                        );
                                    })
                                }
                            </Form.Select>
                        </Form.Group>
                        <br></br>
                        <Form.Group>
                            <Form.Label>Source Node</Form.Label>
                            <Form.Select onChange={(e) => {
                                setSelectedSource(e.target.value);
                                console.log('onChange Source')
                            }}>
                                <option disabled selected>{DEFAULT_SOURCE_NODE}</option>
                                {
                                    getSourceNodes().map((node: string) => {
                                        return <option key={node} value={node}>{node.substring(0, 6)}</option>;
                                    })
                                }
                            </Form.Select>
                        </Form.Group>
                    </Form>
                </Modal.Body>

                <Modal.Footer>
                    {!showAlert ?
                        <>
                            <Button variant="secondary" onClick={handleClose}>
                                Close
                            </Button>
                            <Button
                                variant="primary"
                                disabled={isSimulating}
                                onClick={() => {
                                    console.log(
                                        'isSimulating : ' + isSimulating,
                                        'selectedAlgorithm : ' + selectedAlgorithm,
                                        'selectedSource : ' + selectedSource,
                                    )
                                    checkForValidInput() ? setSimulating(true) : setShowAlert(true)
                                }}
                            >
                                {isSimulating ? 'Simulating…' : 'Simulate'}
                            </Button>
                        </>
                        :
                        <Alert show={showAlert} variant="danger" style={{ width: '100%' }}>
                            <Alert.Heading>Something went wrong!</Alert.Heading>
                            <p>
                                Check for the parameters of the simulation.
                            </p>
                            <hr />
                            <div className="d-flex justify-content-center">
                                <Button onClick={() => setShowAlert(false)} variant="danger">
                                    Close!
                                </Button>
                            </div>
                        </Alert>
                    }
                </Modal.Footer>
            </Modal>
        </>
    );
};

export default SimulationButton;