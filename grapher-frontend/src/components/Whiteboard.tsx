import { FC } from "react";
import "@react-sigma/core/lib/react-sigma.min.css";
import { ControlsContainer, FullScreenControl, SigmaContainer, ZoomControl } from "@react-sigma/core";
import UndirectedGraphComponent from "./UndirectedGraphComponent";
import GraphEvents from "./GraphEvents";
import './Whiteboard.css'
import GraphResetButton from "./GraphResetButton";

const WhiteBoard: FC = () => {

    return (
        <div style={{ width: "100%", height: "100%", padding: "2.5em", maxHeight: "calc(100vh - 8vh)" }}>
            <SigmaContainer className="whiteb">
                <UndirectedGraphComponent />
                <GraphEvents />
                <ControlsContainer position={"top-left"}>
                    <ZoomControl />
                    <FullScreenControl />
                    <GraphResetButton />
                </ControlsContainer>
            </SigmaContainer>
        </div>
    );
};

export default WhiteBoard;