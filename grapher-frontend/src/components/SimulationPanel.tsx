import { FC, MouseEvent, useState } from "react";
import { Card, Offcanvas, ListGroup } from "react-bootstrap";
import NavbarCustom from "./NavbarCustom";
import UndirectedGraphComponent from "./UndirectedGraphComponent";
import { ControlsContainer, FullScreenControl, SigmaContainer, ZoomControl } from "@react-sigma/core";
import "./Whiteboard.css"
import "./SimulationPanel.css"
import SimulationGraph from "./SimulationGraph";
import { useLocation, useNavigate } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faForwardStep } from '@fortawesome/free-solid-svg-icons';

const SimulationPanel: FC = () => {

    const { state } = useLocation();
    const [show, setShow] = useState(false);
    const [cardClicked, setCardClicked] = useState(1);
    const [trigger, setTrigger] = useState(0);
    const [activeIteration, setActiveIteration] = useState<number>(1);
    const [nodeOver, setNodeOver] = useState<string>("")
    const [iteractionOver, setIteractionOver] = useState<number | null>(null)
    let items = [];

    const handleClose = () => {
        const panel = document.querySelector("#sim-panel");
        const navCollapse = document.querySelector("#navCollapse")
        panel?.setAttribute("style", "width: 100%; height: 100%; padding: 2.5em; max-height: calc(100vh - 8vh);");
        navCollapse?.setAttribute("style", "");
        setTrigger(trigger + 1)
        setShow(false);
    }

    const handleShow = () => {
        const panel = document.querySelector("#sim-panel");
        const navCollapse = document.querySelector("#navCollapse")
        panel?.setAttribute("style", "width: calc(100vw - 400px); height: 100%; padding: 2.5em; max-height: calc(100vh - 8vh);");
        navCollapse?.setAttribute("style", "margin-right: 400px;");
        setTrigger(trigger + 1)
        setShow(true);
    }

    function highlightCard(number: number) {
        setCardClicked(number);
        for (let i = 0; i < items.length; i++) {
            if (i + 1 === number)
                document.getElementById("card-" + (i + 1))!.className = "step-card-clicked card";
            else
                document.getElementById("card-" + (i + 1))!.className = "step-card card";
        }
    }

    const handleNodeListMouseOver = (event: MouseEvent<HTMLElement, globalThis.MouseEvent>, node: string, number: number) => {
        if (number > activeIteration) {
            return;
        }
        setTrigger(trigger + 1);
        event.currentTarget.style.color = "orange"
        setNodeOver(node)
    }

    const handleNodeListMouseLeave = (event: MouseEvent<HTMLElement, globalThis.MouseEvent>) => {
        setTrigger(trigger + 1);
        event.currentTarget.style.color = "black"
        setNodeOver("")
    }

    const simObj = Object.keys(state.simulation);

    for (let number = 1; number <= simObj.length; number++) {
        items.push(
            <Card key={number} id={"card-" + number} className={number === cardClicked ? "step-card-clicked" : "step-card"}>
                <button onClick={() => { setActiveIteration(number); highlightCard(number); }} style={{ border: "none", background: "none" }}>
                    <Card.Body>
                        <Card.Title onMouseOver={(event: any) => {
                            if (number > activeIteration) {
                                return;
                            }
                            setTrigger(trigger + 1);
                            setIteractionOver(number)
                            event.currentTarget.style.color = "orange";
                        }} onMouseLeave={(event: any) => {
                            setTrigger(trigger + 1);
                            setIteractionOver(null)
                            event.currentTarget.style.color = "black";
                        }}>Step {number}</Card.Title>
                        <Card.Text>
                            Visited nodes at this step:
                            <ListGroup variant="flush">
                                {
                                    state.simulation[number - 1].map((node: string, index: number) => {
                                        return (
                                            <ListGroup.Item
                                                key={index}
                                                onMouseOver={(event) => handleNodeListMouseOver(event, node, number)}
                                                onMouseLeave={(event) => handleNodeListMouseLeave(event)}>
                                                {node.slice(0, 12)}
                                            </ListGroup.Item>
                                        )
                                    })
                                }
                            </ListGroup>
                        </Card.Text>
                    </Card.Body>
                </button>
            </Card>
        );
    }

    return (
        <>
            <NavbarCustom isInWhiteboard={false}></NavbarCustom>
            <div id="sim-panel" style={{ width: "100%", height: "100%", padding: "2.5em", maxHeight: "calc(100vh - 8vh)" }}>
                <SigmaContainer className="whiteb">
                    <UndirectedGraphComponent />
                    <SimulationGraph activeIteration={activeIteration} state={state} trigger={trigger} nodeOver={nodeOver} iteractionOver={iteractionOver} />
                    <ControlsContainer position={"top-left"}>
                        <ZoomControl />
                        <FullScreenControl />
                        <div className="SimPanelButton" style={{ display: "flex", justifyContent: "center", alignContent: "center" }}>
                            <button style={{
                                padding: "0 0 2 1",
                                background: "none",
                                border: "none"
                            }} onClick={!show ? handleShow : handleClose}>
                                <FontAwesomeIcon icon={faForwardStep} />
                            </button>
                        </div>
                    </ControlsContainer>
                </SigmaContainer>
            </div>
            <Offcanvas show={show} onHide={handleClose} placement="end">
                <Offcanvas.Header closeButton>
                    <Offcanvas.Title>Step by step simulation</Offcanvas.Title>
                </Offcanvas.Header>
                <Offcanvas.Body>
                    {items}
                </Offcanvas.Body>
            </Offcanvas>
        </>
    );
}

export default SimulationPanel;


