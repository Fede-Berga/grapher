import { FC, useContext, useState } from "react";
import { Modal, Nav, Button, Form } from "react-bootstrap";
import GraphContext from "../storage/context";
import { v4 as uuid } from "uuid";

const DownloadButton: FC = () => {
	const [modalDownLoadIsOpen, setModalDownLoadIsOpen] = useState<boolean>(false);
	const [fileFormat, setFileFormat] = useState<string>('json');
	const graphCtx = useContext(GraphContext);

	function chooseFormat() {
		var dataStr = '';
		var attrs = '';
		var dlAnchorElem = document.getElementById('downBtn');
		if (fileFormat === "json") {
			dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(graphCtx.jsonGraph);
			attrs = "graph" + uuid().slice(0, 8) + ".json";
		} else {
			var id = uuid().slice(0, 8);
			var content = "graph " + id + " {\n"
			const obj = JSON.parse(graphCtx.jsonGraph);

			for (var i = 0; i < obj.nodes.length; i++) {
				content += "\t" + obj.nodes[i].key + " [x = " + obj.nodes[i].attributes.x + ", y = " + obj.nodes[i].attributes.y + "];\n"
			}

			for (var j = 0; j < obj.edges.length; j++) {
				content += "\t" + obj.edges[j].source + " -- " + obj.edges[j].target + ";\n";
			}

			content += "\n}"

			dataStr = "data:text/dot;charset=utf-8," + encodeURIComponent(content);
			attrs = "graph" + id + ".dot";
		}

		dlAnchorElem!.setAttribute("href", dataStr);
		dlAnchorElem!.setAttribute("download", attrs);
	}


	return (
		<>
			<Nav.Link onClick={() => setModalDownLoadIsOpen(true)}>
				<dfn data-info="Download">
					<span className="icon">
						<i style={{ color: '#477cff' }} className="fa-solid fa-download"></i>
					</span>
				</dfn>
			</Nav.Link>

			<Modal
				show={modalDownLoadIsOpen}
				onHide={() => setModalDownLoadIsOpen(false)}
				backdrop="static"
				keyboard={false}
				aria-labelledby="contained-modal-title-vcenter"
				centered>

				<Modal.Header closeButton>
					<Modal.Title>Download graph</Modal.Title>
				</Modal.Header>

				<Modal.Body>
					<Form.Label>Choose the file format for your graph.</Form.Label>
					<Form.Select
						onChange={e => { setFileFormat(e.target.value); }}>
						<option value="json">JSON</option>
						<option value="dot">DOT</option>
					</Form.Select>
				</Modal.Body>

				<Modal.Footer>
					<Button variant="primary" type="submit" id="downBtn" href="/" onClick={() => chooseFormat()}>
						Download
					</Button>
				</Modal.Footer>
			</Modal >
		</>
	);
}

export default DownloadButton;