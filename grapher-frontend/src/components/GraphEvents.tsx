import React, { useEffect, useState, useContext, useRef } from "react";
import { v4 as uuid } from "uuid";

import "@react-sigma/core/lib/react-sigma.min.css";
import { useRegisterEvents, useSigma } from "@react-sigma/core";

import GraphContext from "../storage/context";
import { Modal } from "react-bootstrap";

const GraphEvents: React.FC = () => {
    const registerEvents = useRegisterEvents();
    const sigma = useSigma();

    const [draggedNode, setDraggedNode] = useState<string | null>(null);
    const [selectedNode, setSelectedNode] = useState<string | null>(null);
    const [selectedAndNotMovedNode, setSelectedAndNotMovedNode] = useState<string | null>(null);
    const [showAlertNodeAlreadyExixts, setShowAlertNodeAlreadyExixts] = useState<boolean>(false);

    if (!sigma.getCustomBBox()) sigma.setCustomBBox(sigma.getBBox());

    sigma.setSetting('defaultNodeColor', '#23AA23');
    sigma.setSetting('enableEdgeHoverEvents', true);
    sigma.setSetting('enableEdgeClickEvents', true);
    sigma.setSetting('hideLabelsOnMove', true);

    const graphCtx = useContext(GraphContext);

    const firstRenderRef = useRef(false);

    useEffect(() => {
        // Register the events
        registerEvents({
            afterRender: () => {
                if (!firstRenderRef.current && graphCtx.refreshedCtx) {
                    sigma.getGraph().import(JSON.parse(graphCtx.jsonGraph));
                    graphCtx.updateRefreshedCtx(false);
                    firstRenderRef.current = true;
                }
            },
            // Stage events
            clickStage: (e) => {

                if (selectedNode) {
                    setSelectedNode(null)
                }

                // Get coordinates on board
                const pos = sigma.viewportToGraph(e.event);

                // We register the new node into graphology instance
                const id = uuid().replace(/-/gi, '');

                // Create a new node
                const node = {
                    label: id.substring(0, 6),
                    x: pos.x,
                    y: pos.y,
                    size: 12,
                };

                sigma.getGraph().addNode(id, node);
                graphCtx.updateGraph(JSON.stringify(sigma.getGraph().toJSON()));
            },
            // Mouse events
            mouseup: (e) => {

                if (draggedNode) {
                    setDraggedNode(null);
                }
            },
            mousemove: (e) => {
                if (!draggedNode) return;
                // Get new position of node
                setSelectedAndNotMovedNode(null)

                const pos = sigma.viewportToGraph(e);
                sigma.getGraph().setNodeAttribute(draggedNode, "x", pos.x);
                sigma.getGraph().setNodeAttribute(draggedNode, "y", pos.y);

                // Prevent sigma to move camera:
                e.preventSigmaDefault();
                e.original.preventDefault();
                e.original.stopPropagation();

                graphCtx.updateGraph(JSON.stringify(sigma.getGraph().toJSON()));
            },

            // Nodes events
            clickNode: (e) => {

                if (!selectedAndNotMovedNode) {
                    setSelectedNode(null)
                    return
                }

                if (!selectedNode) {
                    setSelectedNode(e.node);
                } else {
                    if (e.node !== selectedNode && sigma.getGraph().hasNode(selectedNode)) {
                        try {
                            sigma.getGraph().addUndirectedEdge(selectedNode, e.node, { size: 7 });
                            graphCtx.updateGraph(JSON.stringify(sigma.getGraph().toJSON()));
                        } catch (error) {
                            setShowAlertNodeAlreadyExixts(true);
                            new Promise((resolve) => setTimeout(resolve, 1000)).then(() => {
                                setShowAlertNodeAlreadyExixts(false)
                            })
                        }
                    }
                    setSelectedNode(null);
                }
            },
            doubleClickNode: (e) => {

                sigma.getGraph().dropNode(e.node);
                e.preventSigmaDefault();
                graphCtx.updateGraph(JSON.stringify(sigma.getGraph().toJSON()));
            },
            downNode: (e) => {

                setSelectedAndNotMovedNode(e.node);
                setDraggedNode(e.node)
            },
            // Edges events
            enterEdge: (e) => {
                sigma.getGraph().setEdgeAttribute(e.edge, "color", "#cc0000");
            },
            leaveEdge: (e) => {
                sigma.getGraph().setEdgeAttribute(e.edge, "color", "#ccc");
            },
            doubleClickEdge: (e) => {
                sigma.getGraph().dropEdge(e.edge);
                e.preventSigmaDefault();
                graphCtx.updateGraph(JSON.stringify(sigma.getGraph().toJSON()));
            },
        });
        firstRenderRef.current = false;
    }, [registerEvents, sigma, draggedNode, selectedNode, selectedAndNotMovedNode, graphCtx, showAlertNodeAlreadyExixts]);

    return (
        <Modal
            size="sm" variant="danger" show={showAlertNodeAlreadyExixts}>
            <Modal.Header>
                <Modal.Title>
                    Edge Already exists!
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>This edge already exists</Modal.Body>
        </Modal>
    );
};

export default GraphEvents;