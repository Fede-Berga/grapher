import { FC, useState } from "react";
import { Button, Carousel, Modal } from "react-bootstrap";
import bg_slide from '../img/bg_carousel_logo.png'
import no_modify from '../img/no_modify.gif'
import offcanva_opening from '../img/offcanva_opening.gif'
import offcanva_usage from '../img/offcanva_usage.gif'

const HowToUseButtonSimulationPanel: FC = () => {
	const [showHowToUse, setsShowHowToUse] = useState<boolean>(false);

	return (
		<>
			<Button variant="outline-primary" onClick={() => setsShowHowToUse(true)}>
				<span className="icon">
					<i className="fa-solid fa-book"></i>
				</span>
				<span>&nbsp;&nbsp;How to use</span>
			</Button>{' '}

			<Modal show={showHowToUse} size="xl" onHide={() => { setsShowHowToUse(false) }}>
				<Modal.Header closeButton>
					<Modal.Title>How to Use Grapher</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Carousel variant="dark">
						<Carousel.Item>
							<img
								style={{
									margin: 'auto',
									textAlign: 'center',
									paddingBottom: "55px"
								}}
								className="d-block w-75"
								src={bg_slide}
								alt="First slide"
							/>
							<Carousel.Caption>
								<p>With the following instructions you can use the simulation tool.</p>
							</Carousel.Caption>
						</Carousel.Item>
						<Carousel.Item>
							<div style={{ margin: 'auto', textAlign: 'center' }}>
								<h3>No modifies</h3><br />
								<p>Inside the simulation tool you cannot modify your graph.</p>
								<img src={no_modify} style={{
									display: 'block',
									marginLeft: 'auto',
									marginRight: 'auto',
									paddingLeft: "100px",
									paddingRight: "100px",
									paddingBottom: "50px",
									width: '100%'
								}} alt="edges_interaction" />
							</div>
						</Carousel.Item>
						<Carousel.Item>
							<div style={{ margin: 'auto', textAlign: 'center' }}>
								<h3>See steps</h3><br />
								<p>You can see step by step simulation of the algorithm</p>
								<img src={offcanva_opening} style={{
									display: 'block',
									marginLeft: 'auto',
									marginRight: 'auto',
									paddingLeft: "100px",
									paddingRight: "100px",
									paddingBottom: "50px",
									width: '100%'
								}} alt="offcanva_opening" />
							</div>
						</Carousel.Item>
						<Carousel.Item>
							<div style={{ margin: 'auto', textAlign: 'center' }}>
								<h3>Highlight nodes</h3><br />
								<p>You can see the exact nodes involved in each step</p>
								<img src={offcanva_usage} style={{
									display: 'block',
									marginLeft: 'auto',
									marginRight: 'auto',
									paddingLeft: "100px",
									paddingRight: "100px",
									paddingBottom: "50px",
									width: '100%'
								}} alt="offcanva_usage" />
							</div>
						</Carousel.Item>
					</Carousel>
				</Modal.Body>
			</Modal>
		</>
	);
}

export default HowToUseButtonSimulationPanel;