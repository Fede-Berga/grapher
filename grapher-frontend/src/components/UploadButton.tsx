import { FC, useContext, useEffect, useState } from "react";
import GraphContext from '../storage/context';
import parseDot from '../utils/Parse_dot';
import verifyJson from '../utils/JsonGraphVerifier';
import { Alert, Button, Modal, Nav } from "react-bootstrap";

const UploadButton: FC = () => {
    const [modalUpLoadIsOpen, setModalUpLoadIsOpen] = useState<boolean>(false);
    const [fileExt, setFileExt] = useState<String>("");
    const [fileContent, setFileContent] = useState<string>("");
    const [isPushedUpload, setIsPushedUpload] = useState<boolean>(false);
    const [isPushedChooseFile, setIsPushedChooseFile] = useState<boolean>(false);
    const [showAlert, setIsShowedAlert] = useState<boolean | undefined>(false);
    const [errorDesc, setErrorDesc] = useState<string>("");
    const graphCtx = useContext(GraphContext);

    function dropHandler(ev: React.DragEvent<HTMLDivElement>) {
        console.log("File dropped");
        ev.preventDefault();

        //Reset border color to black
        const border = document.querySelector('#drop-file');
        if (border)
            border.setAttribute("style", "padding: 50px; border-style: dashed; border-color: black; text-align: center;");

        if (ev.dataTransfer.files.length > 1)
            AlertMsg("You must load a single file.");
        else {
            const file = ev.dataTransfer.files.item(0);
            const fileNameBox = document.querySelector('#file-name-box');
            const fileName = document.querySelector('#file-name-box #file-name');
            if (fileName && fileNameBox && file) {
                fileName.textContent = file.name;
                fileNameBox.setAttribute("style", "display: block;");
                setFileExt(file.name.substring(file.name.indexOf('.'), file.name.length));
                ev.dataTransfer.files.item(0)!.text().then((text) => {
                    setFileContent(text);
                });
            }
        }

    }

    function dragOverHandler(ev: React.DragEvent<HTMLDivElement>) {
        console.log("File(s) in drop zone");
        const border = document.querySelector('#drop-file');
        if (border)
            border.setAttribute("style", "padding: 50px; border-style: dashed; border-color: red; text-align: center;");
        // Prevent default behavior (Prevent file from being opened)
        ev.preventDefault();
    }

    function dragLeaveHandler(ev: React.DragEvent<HTMLDivElement>) {
        console.log("drag leave");
        const border = document.querySelector('#drop-file');
        if (border)
            border.setAttribute("style", "padding: 50px; border-style: dashed; border-color: black; text-align: center;");
    }

    function deleteFile() {
        setFileContent("");
        setFileExt("");
        const fileNameBox = document.querySelector('#file-name-box');
        const fileName = document.querySelector('#file-name-box #file-name');
        const fileInput = document.querySelector('#file-graph input[type=file]') as HTMLInputElement;
        if (fileName && fileNameBox && fileInput) {
            fileName.textContent = "No file selected";
            fileNameBox.setAttribute("style", "display: none;");
            fileInput.value = "";
        }
    }

    function AlertMsg(description: string) {
        setErrorDesc(description);
        setIsShowedAlert(true);
        setTimeout(() => {
            setIsShowedAlert(false);
        }, 3000);
    }

    function shakeTrash() {
        const non_shaking_trash = document.querySelector('#delete-file #non-shaking-trash');
        const shaking_trash = document.querySelector('#delete-file #shaking-trash');

        non_shaking_trash!.setAttribute("style", "display: none; color: red;");
        shaking_trash!.setAttribute("style", "display: block; color: red");
    }

    function stopShakeTrash() {
        const non_shaking_trash = document.querySelector('#delete-file #non-shaking-trash');
        const shaking_trash = document.querySelector('#delete-file #shaking-trash');

        non_shaking_trash!.setAttribute("style", "display: block; color: red;");
        shaking_trash!.setAttribute("style", "display: none; color: red");
    }

    useEffect(() => {
        if (isPushedChooseFile) {
            const uploadFile = () => {
                const fileInput = document.querySelector('#file-graph input[type=file]') as HTMLInputElement;
                if (fileInput)
                    fileInput.onchange = () => {
                        if (fileInput.files && fileInput.files.length > 0) {
                            const file = fileInput.files[0];
                            const fileNameBox = document.querySelector('#file-name-box');
                            const fileName = document.querySelector('#file-name-box #file-name');
                            if (fileNameBox && fileName) {
                                fileName.textContent = file.name;
                                fileNameBox.setAttribute("style", "display: block;");
                                setFileExt(file.name.substring(file.name.indexOf('.'), file.name.length));
                                file.text().then((text) => {
                                    setFileContent(text);
                                });
                            }
                        }
                    }
            };

            uploadFile();
            setIsPushedChooseFile(false);
        }

        if (isPushedUpload) {
            const pushGraph = () => {
                if (fileExt === ".json") {
                    const verifyJsonRes = verifyJson(fileContent);
                    if (verifyJsonRes.status) {
                        graphCtx.updateGraph(fileContent);
                        graphCtx.updateRefreshedCtx(true);
                        setModalUpLoadIsOpen(false);
                    } else
                        AlertMsg(verifyJsonRes.err_descr);
                } else if (fileExt === ".dot") {
                    const verifyDotRes = parseDot.verifyDot(fileContent);
                    if (verifyDotRes.status) {
                        var jParsedGraph = parseDot.getParsedGraph();
                        console.log(jParsedGraph);
                        graphCtx.updateGraph(jParsedGraph);
                        graphCtx.updateRefreshedCtx(true);
                        setModalUpLoadIsOpen(false);
                    } else
                        AlertMsg(verifyDotRes.err_descr);
                }
                else {
                    if (fileContent === "")
                        AlertMsg("You must select a file.");
                    else if (fileExt !== ".dot" && fileExt !== ".json") {
                        AlertMsg("File format not supported, it must be a .json or a .dot.");
                    }
                }
            };

            pushGraph();
            setIsPushedUpload(false);
        }
    }, [graphCtx, isPushedUpload, isPushedChooseFile, fileContent, fileExt]);


    return (
        <>
            <Nav.Link role="up-open-modal" id="upLoad" onClick={() => setModalUpLoadIsOpen(true)}>
                <dfn data-info="Upload">
                    <span className="icon">
                        <i style={{ color: '#477cff' }} className="fa-solid fa-file-import"></i>
                    </span>
                </dfn>
            </Nav.Link>

            <Modal show={modalUpLoadIsOpen}
                onHide={() => { setModalUpLoadIsOpen(false); setFileContent(""); setFileExt(""); }}
                backdrop="static"
                keyboard={false}>
                <Modal.Header closeButton>
                    <Modal.Title>Upload a file</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <p style={{ paddingBottom: '10px' }}>Upload a file to the Grapher platform in order to visualize the graph.</p>

                    <div id="drop-file" style={{ padding: '50px', borderStyle: 'dashed', borderColor: 'black', textAlign: 'center' }}
                        onDrop={(event) => dropHandler(event)}
                        onDragOver={(event) => dragOverHandler(event)}
                        onDragLeave={(event) => dragLeaveHandler(event)}>
                        Drop a file here
                    </div>

                    <p style={{ paddingTop: '10px', textAlign: 'center' }}><b>Or:</b></p>


                    <div id="file-graph" style={{ paddingBottom: '10px', display: 'flex', justifyContent: 'center' }} className="file is-boxed">
                        <label className="file-label">
                            <input className="file-input"
                                data-testid="choose-file-button"
                                id='file-input'
                                type="file"
                                accept='.dot,.json'
                                name="resume"
                                onClick={() => setIsPushedChooseFile(true)} />
                            <span className="file-cta">
                                <span className="file-icon">
                                    <i className="fas fa-upload"></i>
                                </span>
                                <span className="file-label">
                                    Choose a file
                                </span>
                            </span>
                        </label>
                    </div>

                    <div id="file-name-box" className='box' style={{ display: 'none' }}>
                        <span className="icon">
                            <i className="fa fa-file"></i>
                        </span>
                        <span id="file-name">No file selected</span>
                        <Button id="delete-file"
                            style={{ float: 'right', border: 'none' }}
                            variant="white" onClick={() => deleteFile()}
                            onMouseOver={() => shakeTrash()}
                            onMouseLeave={() => stopShakeTrash()}>
                            <i id="non-shaking-trash" style={{ color: 'red', display: 'block' }} className="fa-regular fa-trash-can"></i>
                            <i id="shaking-trash" style={{ color: 'red', display: 'none' }} className="fa-regular fa-trash-can fa-shake"></i>
                        </Button>
                    </div>

                    <Alert id="error-alert"
                        show={showAlert}
                        onClose={() => setIsShowedAlert(false)}
                        variant="danger"
                    >
                        <Alert.Heading>Error!</Alert.Heading>
                        <p>
                            {errorDesc}
                        </p>
                    </Alert>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="success"
                        data-testid="upload-button"
                        style={{ float: 'right' }}
                        onClick={() => { setIsPushedUpload(true); }}>
                        Upload
                    </Button>
                </Modal.Footer>
            </Modal >
        </>
    );
}

export default UploadButton;