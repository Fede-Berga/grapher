import axios from "axios";
import GraphContext from "../storage/context";
import { useContext, useEffect, useRef } from 'react';
import { useNavigate, useParams } from 'react-router-dom';

const BASE_URL = 'http://127.0.0.1:5001';

function GraphCloudLoader() {
  const graphCtx = useContext(GraphContext);
  const dataFetched = useRef(false);
  let params = useParams();
  const graphId = params.graphId
  const navigate = useNavigate();

  const fetchData = () => {
    console.log(graphId);
    if (graphId) {
      axios.get("/graph/" + graphId, {
        baseURL: BASE_URL,
      })
        .then((response) => {
          if (response.data.status === "ok") {
            graphCtx.updateGraph(response.data.response);
            graphCtx.updateRefreshedCtx(true);
            navigate("/");
          }
        })
        .catch((err) => {
          console.log(err.response.data.response)
          alert("Error, graph not found");
          navigate("/");
        });
    }
  }

  useEffect(() => {
    if (dataFetched.current) return;
    dataFetched.current = true;
    fetchData();
  })

  return null;
}

export default GraphCloudLoader;