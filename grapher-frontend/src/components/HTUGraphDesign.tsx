import { FC, useState } from "react";
import { Modal, Button, Carousel } from "react-bootstrap";
import edges_interaction from '../img/edge_interact.gif'
import nodes_interaction from '../img/node_interact.gif'
import upload_file from '../img/upload_file.gif'
import download_file from '../img/download_file.gif'
import bg_slide from '../img/bg_carousel_logo.png'
import simulating from '../img/simulating.gif'
import upload_cloud from '../img/upload_cloud.gif'
import resetWtb from '../img/clean_whiteboard.gif'

const HTUGraphDesign: FC = () => {
	const [modalIsOpen, setModalIsOpen] = useState<boolean>(false);

	return (
		<>
			<Button variant="outline-primary" onClick={() => setModalIsOpen(true)}>
				<span className="icon">
					<i className="fa-solid fa-book"></i>
				</span>
				<span>&nbsp;&nbsp;How to use</span>
			</Button>{' '}


			<Modal
				show={modalIsOpen}
				onHide={() => setModalIsOpen(false)}
				size="xl"
				backdrop="static"
				keyboard={false}
				aria-labelledby="contained-modal-title-vcenter"
				centered>

				<Modal.Header closeButton>
					<Modal.Title>How to use Grapher</Modal.Title>
				</Modal.Header>

				<Modal.Body>
					<Carousel variant="dark">
						<Carousel.Item>
							<img
								style={{
									margin: 'auto',
									textAlign: 'center',
									paddingBottom: "55px"
								}}
								className="d-block w-100"
								src={bg_slide}
								alt="First slide"
							/>
							<Carousel.Caption>
								<p>With the following instructions you can draw an undirected graph on the whiteboard</p>
							</Carousel.Caption>
						</Carousel.Item>
						<Carousel.Item>
							<div style={{ margin: 'auto', textAlign: 'center' }}>
								<h3>Nodes interaction</h3><br />
								<p>Click on stage to add a node <br /> Double click on a node to delete it</p>
								<img src={nodes_interaction} style={{
									display: 'block',
									marginLeft: 'auto',
									marginRight: 'auto',
									width: '75%'
								}} alt="nodes_interaction" />
							</div>
						</Carousel.Item>
						<Carousel.Item>
							<div style={{ margin: 'auto', textAlign: 'center' }}>
								<h3>Edges interaction</h3><br />
								<p>Click on a node and then another one to create an edge between them
									<br />Double click on an edge to delete it</p>
								<img src={edges_interaction} style={{
									display: 'block',
									marginLeft: 'auto',
									marginRight: 'auto',
									width: '100%'
								}} alt="edges_interaction" />
							</div>
						</Carousel.Item>
						<Carousel.Item>
							<div style={{ margin: 'auto', textAlign: 'center' }}>
								<h3>Upload graph</h3><br />
								<p>Click on the upload icon, then select your file and push upload</p>
								<img src={upload_file} style={{
									display: 'block',
									marginLeft: 'auto',
									marginRight: 'auto',
									marginBottom: '35px',
									width: '75%'
								}} alt="upload_file" />
							</div>
						</Carousel.Item>
						<Carousel.Item>
							<div style={{ margin: 'auto', textAlign: 'center' }}>
								<h3>Download a graph</h3><br />
								<p>Click on the download icon, then select your favorite file format and download it</p>
								<img src={download_file} style={{
									display: 'block',
									marginLeft: 'auto',
									marginRight: 'auto',
									marginBottom: '35px',
									width: '75%'
								}} alt="download_file" />
							</div>
						</Carousel.Item>
						<Carousel.Item>
							<div style={{ margin: 'auto', textAlign: 'center' }}>
								<h3>Simulate an algorithm</h3><br />
								<p>You can choose an algorithm to simulate on your graph.</p>
								<img src={simulating} style={{
									display: 'block',
									marginLeft: 'auto',
									marginRight: 'auto',
									marginBottom: '35px',
									width: '75%'
								}} alt="simulating" />
							</div>
						</Carousel.Item>

						<Carousel.Item>
							<div style={{ margin: 'auto', textAlign: 'center' }}>
								<h3>Upload/restore you graph to/from cloud</h3><br />
								<p>You can upload your graph to cloud in order to restore it in the future.</p>
								<img src={upload_cloud} style={{
									display: 'block',
									marginLeft: 'auto',
									marginRight: 'auto',
									marginBottom: '35px',
									width: '75%'
								}} alt="upload_cloud" />
							</div>
						</Carousel.Item>
						<Carousel.Item>
							<div style={{ margin: 'auto', textAlign: 'center' }}>
								<h3>Delete whole graph</h3><br />
								<p>You can delete whole graph from the whiteboard.</p>
								<img src={resetWtb} style={{
									display: 'block',
									marginLeft: 'auto',
									marginRight: 'auto',
									marginBottom: '35px',
									width: '75%'
								}} alt="reset whiteboard" />
							</div>
						</Carousel.Item>
					</Carousel>
				</Modal.Body>
			</Modal>
		</>
	);
}

export default HTUGraphDesign;