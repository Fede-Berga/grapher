import { FC, useEffect } from "react";

import "@react-sigma/core/lib/react-sigma.min.css";
import { UndirectedGraph } from "graphology";
import { useLoadGraph } from "@react-sigma/core";

const UndirectedGraphComponent: FC = () => {
  const loadGraph = useLoadGraph();

  useEffect(() => {
    // Create the graph
    const graph = new UndirectedGraph();
    loadGraph(graph);
  }, [loadGraph]);

  return null;
};

export default UndirectedGraphComponent;