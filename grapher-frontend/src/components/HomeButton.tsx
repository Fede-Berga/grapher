import { FC, useContext } from "react";
import { Nav } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import GraphContext from "../storage/context";

const HomeButton: FC = () => {
    
    const navigate = useNavigate();
    const graphCtx = useContext(GraphContext);

    return(
        <>
            <Nav.Link onClick={() => {navigate('/'); graphCtx.updateRefreshedCtx(true)}}>
                <dfn data-info="Back to home">
                    <span className="icon">
                        <i style={{ color: '#477cff' }} className="fa-solid fa-house"></i>
                    </span>
                </dfn>
            </Nav.Link>
        </>
    );
};

export default HomeButton;