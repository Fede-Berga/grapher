import './App.css';
import Whiteboard from './components/Whiteboard';
import NavbarCustom from './components/NavbarCustom';

function App() {

  return (
    <>
      <NavbarCustom isInWhiteboard={true} />
      <Whiteboard />
    </>
  );
}

export default App;