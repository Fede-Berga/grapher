import { parse } from '@ts-graphviz/parser';
import { INode } from 'ts-graphviz';
import { v4 as uuid } from "uuid";

class nodeBuilder {
    jNode;

    constructor() {
        this.jNode = {
            key: "",
            attributes: {
                label: "",
                x: 0,
                y: 0,
                size: 12
            }
        };
    }

    setKey(key: string) {
        this.jNode.key = key;
    }

    setX(x: number) {
        this.jNode.attributes.x = x;
    }

    setY(y: number) {
        this.jNode.attributes.y = y;
    }

    setLabel(){
        this.jNode.attributes.label = this.getKey().substring(0, 6);
    }

    getKey() {
		return this.jNode.key;
	}

    toString() {
        return JSON.stringify(this.jNode);
    }
}

class edgeBuilder {
    jEdge;

    constructor() {
        this.jEdge = {
            key: uuid().slice(0, 8).replaceAll("-", ""),
            source: "",
            target: "",
            attributes: {
                size: 7
            },
            undirected: true
        };
    }

    setSource(s: string) {
        this.jEdge.source = s;
    }

    setTarget(t: string) {
        this.jEdge.target = t;
    }

    getSource() {
		return this.jEdge.source;
	}

	getTarget() {
		return this.jEdge.source;
	}

    toString() {
        return JSON.stringify(this.jEdge);
    }
}

class jsonGraphBuilder {
    jsonGraph: { options: Object, attributes: Object, nodes: Object[], edges: Object[] };

    constructor() {
        this.jsonGraph = {
            options: {
                type: "undirected",
                multi: false,
                allowSelfLoops: false
            },
            attributes: {},
            nodes: [],
            edges: []
        };
    }

    addNode(node: INode | undefined) {
        var nodeObj = new nodeBuilder();

        if (node !== undefined) {
            //Extracting id and coordinates
            nodeObj.setKey(node.id);
            //the object node.attributes.values is in this form [["x", "0..."], ["y", "0..."]]
            var xArr = node.attributes.values.at(0);
            var yArr = node.attributes.values.at(1);
            if (xArr && yArr) {
                nodeObj.setX(+xArr[1]); //+ for casting from string to number
                nodeObj.setY(+yArr[1]);
            }

            nodeObj.setLabel();

            //Adding the nodeObj to the nodes array in jsonBaseGraph
            this.jsonGraph.nodes.push(nodeObj.jNode);
        }
    }

    addEdge(source: string, target: string) {
        var edgeObj = new edgeBuilder();
        edgeObj.setSource(source);
        edgeObj.setTarget(target);

        this.jsonGraph.edges.push(edgeObj.jEdge);
    }

    toString() {
        return JSON.stringify(this.jsonGraph);
    }
}

function checkEdges(G: any) {
    let nodes = new Set();
    let edges = new Set();

    //Collect all nodes
    for (let i = 0; i < G.nodes.length; i++)
        nodes.add(G.nodes[i].id);
    //Collect nodes in edges
    for (let i = 0; i < G.edges.length; i++) {
        let targets = G.edges[i].targets;
        if (targets.length > 1) {
            for (let j = 0; j < targets.length; j++)
                edges.add(targets[j].id);
        }
    }

    if (nodes.size < edges.size)
        return false;

    for (var it = edges.values(), val = it.next().value; val != null; val = it.next().value) {
        if (!nodes.has(val))
            return false;
    }
    return true;
}

class parseDot {
    static G: any;

    static verifyDot(raw_text: string) {
        let result = {
            status: false,
            err_descr: "",
        }
        try {
            this.G = parse(raw_text);
        } catch (e) {
            result.err_descr = "Error in parsing the dot file: " + e;
            return result;
        }

        if(raw_text.trim().substring(0, 5) !== "graph"){
            result.err_descr = "The specified graph must start with 'graph'!";
            return result;
        }

        for (let i = 0; i < this.G.nodes.length; i++) {
            if(this.G.nodes.at(i).attributes.values.at(0) === undefined || this.G.nodes.at(i).attributes.values.at(1) === undefined){
                result.err_descr = "All nodes must have x and y coordinates!";
                return result;
            }
        }

        //check if there are nodes in the 'edges' that are not specified in the 'nodes'
        if (!checkEdges(this.G)) {
            result.err_descr = "There are edges that have non-existent nodes";
            return result;
        }
        result.status = true;
        return result;
    }

    static getParsedGraph() {
        var jGraph = new jsonGraphBuilder();
        //reading nodes
        for (let i = 0; i < this.G.nodes.length; i++) {
            jGraph.addNode(this.G.nodes.at(i));
        }

        //reading edges
        for (let i = 0; i < this.G.edges.length; i++) {
            let targets = this.G.edges[i].targets;
            if (targets.length > 1) {
                for (let j = 0; j < targets.length - 1; j++) {
                    jGraph.addEdge(targets[j].id, targets[j + 1].id);
                }
            }
        }

        return jGraph.toString();
    }

}

export default parseDot;
