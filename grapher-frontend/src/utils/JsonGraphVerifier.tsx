import Ajv from 'ajv';
import schema from "./schema.json";

function validateJSONSchema(jsonGraph: any) {
    const ajv = new Ajv();
    return ajv.validate(schema, jsonGraph);
}

function checkEdges(jsonGraph: any){
    let nodes = new Set();
    let edges = new Set();

    //Collect all nodes
    for (let i = 0; i < jsonGraph.nodes.length; i++)
        nodes.add(jsonGraph.nodes[i].key);

    //Collect all source/target in edges
    for (let i = 0; i < jsonGraph.edges.length; i++) {
        edges.add(jsonGraph.edges[i].source);
        edges.add(jsonGraph.edges[i].target);
    }
    
    if(nodes.size < edges.size)
        return false;
    
    for(var it=edges.values(), val = it.next().value; val != null; val = it.next().value){
        if(!nodes.has(val))
            return false;
    }
    return true;
}

//s is the string that should contain a well written graph in JSON format
function verifyJson(s:string){

    let parsedJson;
    let result = {
        status: false,
        err_descr: "",
    }

    //check the file syntax 
    try{
        parsedJson = JSON.parse(s);
    } catch(e){
        result.err_descr = "Error in parsing the json file: " + e;
        return result;
    }

    //check if schema is respected
    if(!validateJSONSchema(parsedJson)){
        result.err_descr = "Json schema not valid!";
        return result;
    }

    //check if there are target or sources in the 'edges' that are not specified in the 'nodes'
    if(!checkEdges(parsedJson)){
        result.err_descr = "There are edges that have non-existent nodes";
        return result;
    }

    result.status = true;
    return result;
}

export default verifyJson;