import { createContext, useState } from "react";

const GraphContext = createContext({
	jsonGraph: '{}',
	refreshedCtx: false,
	updateGraph: (graph) => { },
	updateRefreshedCtx: (x) => { },
});


export const GraphContextProvider = (props) => {
	const [jsonGraphState, setJsonGraphState] = useState('{}');
	const [refreshedtCtxState, setRefreshedCtxState] = useState(false);

	function updateGraphHandler(graph) {
		setJsonGraphState(prevState => {
			return graph;
		});
	}

	function updateRefreshedCtxHandler(x){
		setRefreshedCtxState(prevState => {
			return x;
		});
	}

	const context = {
		jsonGraph: jsonGraphState,
		refreshedCtx: refreshedtCtxState,
		updateGraph: updateGraphHandler,
		updateRefreshedCtx: updateRefreshedCtxHandler,
	};

	return (
		<GraphContext.Provider value={context}>
			{props.children}
		</GraphContext.Provider>
	);
};

export default GraphContext;
