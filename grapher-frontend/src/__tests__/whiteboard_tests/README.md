# Whiteboard Testing

In this section are performed unit tests to verify the correctness of the *Whiteboard* component.

## Unit Testing Whiteboard

The *unit tests* are described in the [Whiteboard.test.tsx](Whiteboard.test.tsx).
The rendering of the component is tested.

## End-to-End test testing

Given the incompatibilty of the [Sigma](https://sim51.github.io/react-sigma/) library with the [Selenium IDE](https://www.selenium.dev/selenium-ide/) environment, we proceed with manual End to End testing for the whiteboard.

## Docker compose
You can also use docker-compose file instead run ```npm start``` command

### Multigraph testing

This test verifies if an alert is triggered after the atempt of creating a multigraph.

1. Start the app with ``` npm start ```
2. Head to ``` localhost:3000/ ```
3. Draw a node with a single click.
4. Draw a sencond node.
5. Connect the two crated node clicking on the first and the second.
6. repeat point 5.
7. An alert should display.

### Self link testing

No self link should exist.

1. Start the app with ``` npm start ```
2. Head to ``` localhost:3000/ ```
3. Draw a node with a single click.
4. Click (slowly) twice on the just created node.
5. No edge shoud be rendered.

### Edge deletion after node deletion

When a node is deleted, every edge adjacent to it shoud be deleted as well.

1. Start the app with ``` npm start ```
2. Head to ``` localhost:3000/ ```
3. Create the complete graph with four nodes.
4. Cancel one of the nodes
5. Every edge adjacent to it shall be removed as well.


### Delete whole graph from the whiteboard

Clicking the trash icon the graph should be deleted

1. Start the app with ``` npm start ```
2. Head to ``` localhost:3000/ ```
3. Create a simple graph with some nodes and edges
4. Click on the trash icon on the left side of the whiteboard in order to delete whole graph
5. You will see that everything on the whiteboard will be deleted 