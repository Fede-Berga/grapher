import TestRenderer from 'react-test-renderer';
import WhiteBoard from '../../components/Whiteboard';
import Adapter from 'enzyme-adapter-react-16'
import { shallow, configure } from 'enzyme';
import GraphResetButton from '../../components/GraphResetButton';


describe('Whiteboard rendering', () => {
        it('Should correctly render the whiteboard', () => {
                const testRenderer = TestRenderer.create(<WhiteBoard />);
                const testInstance = testRenderer.root;
                expect(testInstance.findByProps({ className: "whiteb" })).toBeTruthy();
        });
});


configure({ adapter: new Adapter() });

describe('Whiteboard reset icon', () => {
        it('Should correctly render the reset button icon', () => {
                const wrapper = shallow(<WhiteBoard />);
                expect(wrapper.find(GraphResetButton)).toHaveLength(1);
        });
});