# Testing the application

Several testing scenarios have been thought for the application.

In the following page you will find a short description of the kind of tests performed together with how to run them and a link pointing to the relative folders in the repo to get further details.

## Front-end Unit Tests / Integration Tests

If you are interested in testing the UI of Grapher you can run the following command:

```sh
cd grapher-frontend/
npm run test:ci
```

By running this command you can run the **Unit tests** to check the correct behaviour of the following components and functionalities:

- Navbar rendering ([Go to the folder](https://gitlab.com/Fede-Berga/grapher/-/tree/main/grapher-frontend/src/__tests__/navbar_tests))
- Whiteboard rendering ([Go to the folder](https://gitlab.com/Fede-Berga/grapher/-/tree/main/grapher-frontend/src/__tests__/whiteboard_tests))
- Save graph as a *JSON* ([Go to the folder](https://gitlab.com/Fede-Berga/grapher/-/tree/main/grapher-frontend/src/__tests__/json_graph_tests))
- Save graph as a *DOT* ([Go to the folder](https://gitlab.com/Fede-Berga/grapher/-/tree/main/grapher-frontend/src/__tests__/dot_graph_tests))
- Simulation Panel ([Go to the folder](https://gitlab.com/Fede-Berga/grapher/-/tree/main/grapher-frontend/src/__tests__/simulation_panel_tests))

## Front-end E2E tests

By running these commands you can run **E2E** tests to check the correct UI interaction of the following component:

- Navbar interaction ([Go to the folder](https://gitlab.com/Fede-Berga/grapher/-/tree/main/grapher-frontend/src/__tests__/navbar_tests))
- Simulation Panel interaction([Go to the folder](https://gitlab.com/Fede-Berga/grapher/-/tree/main/grapher-frontend/src/__tests__/simulation_panel_tests))

```sh
    npm i -g selenium-side-runner
    selenium-side-runner ./path/to/file.side 
```
Use sudo if you are using a Linux distro before *npm*.

- Upload/restore graph to/from cloud ([Go to the folder](https://gitlab.com/Fede-Berga/grapher/-/tree/main/grapher-frontend/__test__/cloud_test))

