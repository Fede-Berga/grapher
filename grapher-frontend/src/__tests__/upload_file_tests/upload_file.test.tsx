import { act, fireEvent, render, screen, waitFor } from "@testing-library/react"
import userEvent from "@testing-library/user-event"
import NavbarCustom from "../../components/NavbarCustom"


test('Open the upload modal when clicking on the icon', async () => {
    const user = userEvent.setup();
    act(() => {
      render(<NavbarCustom isInWhiteboard = {true}/>);
    });
    
    await act(async () => {
      await user.click(screen.getByRole('up-open-modal'));
    });
    expect(screen.getByText("Upload a file")).toBeInTheDocument();
    expect(screen.getByTestId("upload-button")).toBeInTheDocument();
    expect(screen.getByTestId("choose-file-button")).toBeInTheDocument();
  });

test('Upload a file, in particular a .json', async() => {
    const user = userEvent.setup();
    let file = new File(["test"], "test.json", { type: "application/json" });
    // render the component
    render(<NavbarCustom isInWhiteboard = {true}/>);

    await act(async () => {
      await user.click(screen.getByRole('up-open-modal'));
    });

    // get the upload button
    let uploader = screen.getByTestId("choose-file-button");
    // simulate upload event and wait until finish
    await waitFor(() =>
      fireEvent.change(uploader, {
        target: { files: [file] },
      })
    );
    // get the same uploader from the dom
    let input = document.getElementById("file-input") as HTMLInputElement;
    // check if the file is there
    expect(input.files?.[0].name).toBe("test.json");
    expect(input.files?.length).toBe(1);
});