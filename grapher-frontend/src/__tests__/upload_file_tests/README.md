# Upload Testing

In this section are performed e2e tests to verify the correctness of the *Upload* functionality.

## End-to-End test testing
Since [Selenium IDE](https://www.selenium.dev/selenium-ide/) has problems with relative path, we proceeded with manual End to End testing for the upload functionality.

Before all the following tests you have to start the application with these commands:
1. Start the app with ``` npm start ```
2. Head to ``` localhost:3000/ ```
3. Click on the upload icon at the top right of the screen

### Accepted JSON file
This test shows the behavior of the platform when you upload a correct **.json** file that represents a graph.

1. Select or drag and drop the file named *accepted.json*.
2. Click on 'Upload'.
3. You can see the L-shaped graph on the whiteboard.

### Accepted DOT file first version.
This test shows the behavior of the platform when you upload a correct **.dot** file that represents a graph.
The 3 nodes are written inside the file like this: <br>
*x -- y;<br>
  y -- x;*

1. Select or drag and drop the file named *accepted.dot*.
2. Click on 'Upload'.
3. You can see the L-shaped graph on the whiteboard.

### Accepted DOT file second version
This test shows the behavior of the platform when you upload a correct **.dot** file that represents a graph.
The difference between this and the previous test is that in this case the 3 nodes that will be displayed are written in one single row inside the file, like this:<br> 
*x -- y -- z;*

1. Select or drag and drop the file named *accepted.dot*.
2. Click on 'Upload'.
3. You can see the L-shaped graph on the whiteboard.

### Invalid JSON/DOT file
This test shows the behavior of the platform when you upload a **.json** or **.dot** file with syntax error(s).

1. Select or drag and drop the file named *invalid.json* or *invalid.dot*.
2. Click on 'Upload'.
3. You can see an alert that warns you about a syntax error.

### Invalid JSON schema
This test shows the behavior of the platform when you upload a **.json** file with an invalid schema.

1. Select or drag and drop the file named *invalid_schema.json*.
2. Click on 'Upload'.
3. You can see an alert that warns you about a schema error in the file.

### Non-existent node inside edges JSON/DOT file
This test shows the behavior of the platform when you upload a **.json** or **.dot** file that contains a graph that has a non-existent node inside the list of edges.

1. Select or drag and drop the file named *non-exist_nodes.json* or *non-exist_nodes.dot*.
2. Click on 'Upload'.
3. You can see an alert that warns you about the non-existence of a node inside the list of edges.

### Nodes x and y coordinates in DOT file
This test shows the behavior of the platform when you upload a **.dot** file that contains at least a node that does not have *x* and *y* properties.

1. Select or drag and drop the file named *x_y_prop.dot*.
2. Click on 'Upload'.
3. You can see an alert that warns you about a missing *x* and *y* properties in at least a node

### Not an undirected graph in DOT file
This test shows the behavior of the platform when you upload a **.dot** file that does not begin with *'graph'*.

1. Select or drag and drop the file named *not_undirected.dot*.
2. Click on 'Upload'.
3. You can see an alert that warns you about the fact that the file does not start with *'graph'*

### Uploading a file that is neither a JSON nor a DOT file
This test shows the behavior of the platform when you upload a file that is neither a JSON nor a DOT file.

1. Drag and drop the file named *not_json_dot.txt*.
2. Click on 'Upload'.
3. You can see an alert that warns you about the fact that the file must be eaither a **.dot** file or a **.json**.