# Navbar rendering

In this section is tested the correct Navbar component rendering.
This means that the title *Grapher* and the button with text *How to use* are shown correctly.

## Unit tests

These tests are performed in the [Navbar.test.tsx](./Navbar.test.tsx) file. They verify the correct rendering and interaction with the components.

## End-to-End tests

These tests are performed in the [grapher.side](./grapher.side) file. They verify the correct interaction of the user with the component.
