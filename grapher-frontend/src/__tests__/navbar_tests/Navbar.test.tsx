import { render, screen } from '@testing-library/react';
import NavbarCustom from '../../components/NavbarCustom';

describe('Main Logo Rendering', () => {
	it('Should render the main logo', () => {
		render(<NavbarCustom isInWhiteboard = {true}/>)
		expect(screen.getByText("Grapher")).toBeInTheDocument();
	});
});

describe('/"How to use/" button Testing', () => {
	it('should render /"How to use/" button when loaded', () => {
		render(<NavbarCustom isInWhiteboard = {true}/>)
		expect(screen.getByText("How to use")).toBeInTheDocument();
	});
});