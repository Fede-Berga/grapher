# Upload/restore graph to/from cloud

We can not perform automated E2E tests due to an incompatibility with the selenium test tools, so here the steps to follow in order to reproduce the right behaviour:
- Click on the upload to cloud icon at the top right of the screen;
- Then if the upload operation has been successful, save the showed URL;
- Copy the URL on your browser in order to restore your graph.  

![Alt Text](../../img/upload_cloud.gif)