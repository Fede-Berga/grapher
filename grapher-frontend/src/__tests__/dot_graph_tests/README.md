# Save graph as a *DOT* (Semi-automatic Test)

In this section is tested the correct generation of the *DOT* file representing the graph drawn on the Whiteboard.


It's considered as a semi-automatic test because the graph has been drawn and downloaded manually from grapher because of incompatibilites with automatic GUI test tools.

Here an image of the graph tested:
![graph](./imgs/dot_test.png)
And follows the *DOT* example schema representing a graph:

```dot
graph G {
	ab589532ac8440779bf152395e6edc5e [x = 0.13012476412175272, y = 0.7718359975807141];
	0f9e62e5a86b45ecad6c28812b8acfac [x = 0.124777170350019, y = 0.33333333918821795];
	05f1d33d29354ffeb68ed0266fc1f644 [x = 0.6149732660922745, y = 0.32798574579318746];
	ab589532ac8440779bf152395e6edc5e -- 0f9e62e5a86b45ecad6c28812b8acfac;
	0f9e62e5a86b45ecad6c28812b8acfac -- 05f1d33d29354ffeb68ed0266fc1f644;

}
```

The following features have been automatically checked:

- The number of nodes in the *DOT* file is equal to the drawn graph;
- The number of edges in the *DOT* file is equal to the drawn graph;
- The nodes' ID are unique;
- The edges' ID are unique;
- There are not self loops on nodes;
- In the edges' source and target fields, there are actual nodes' IDs.