import parseDot from '../../utils/Parse_dot';
import path from 'path';
import fs from 'fs';


// Import del file scaricato dalla piattaforma
const graphDotPath = path.join(__dirname, './graph.dot');
const graphDot = fs.readFileSync(graphDotPath, 'utf-8');

// Parsing 
parseDot.verifyDot(graphDot);
const jParsedGraph = parseDot.getParsedGraph();
const jsonGraph = JSON.parse(jParsedGraph);

let idNodesSet = new Set();
let idEdgesSet = new Set();

function hasSpecificNumNodes(num: number) {
	return jsonGraph.nodes.length == num;
}

function hasSpecificNumEdges(num: number) {
	return jsonGraph.edges.length == num;
}

function hasUniqueNodeId(num: number) {
	for (let i = 0; i < jsonGraph.nodes.length; i++) {
		idNodesSet.add(jsonGraph.nodes[i].key);
	}
	return idNodesSet.size;
}

function hasUniqueEdgesId(num: number) {
	for (let i = 0; i < jsonGraph.edges.length; i++) {
		idEdgesSet.add(jsonGraph.edges[i].key);
	}
	return idEdgesSet.size;
}


function hasNotNodesSelfloop() {
	for (let i = 0; i < jsonGraph.edges.length; i++) {
		if (jsonGraph.edges[i].source === jsonGraph.edges[i].target)
			return false;
	}
	return true;
}

function existNodesEdge() {
	for (let i = 0; i < jsonGraph.edges.length; i++) {
		if (!idNodesSet.has(jsonGraph.edges[i].source) && !idNodesSet.has(jsonGraph.edges[i].target))
			return false;
	}
	return true;
}

describe('hasSpecificNumNodes', () => {
	it('should pass (3 nodes)', () => {
		const NodeNum = 3;
		const actual = hasSpecificNumNodes(NodeNum);
		expect(actual).toBeTruthy();
	});
});

describe('hasSpecificNumEdges', () => {
	it('should pass (2 edges)', () => {
		const EdgesNum = 2;
		const actual = hasSpecificNumEdges(EdgesNum);
		expect(actual).toBeTruthy();
	});
});

describe('hasUniqueNodesId', () => {
	it('should pass (3 nodes)', () => {
		const NodesNum = 3;
		const actual = hasUniqueNodeId(NodesNum);
		expect(actual).toBeTruthy();
	});
});

describe('hasUniqueEdgesId', () => {
	it('should pass (2 edges)', () => {
		const EdgesNum = 2;
		const actual = hasUniqueEdgesId(EdgesNum);
		expect(actual).toBeTruthy();
	});
});

describe('hasNotNodesSelfloop', () => {
	it('should pass', () => {
		const actual = hasNotNodesSelfloop();
		expect(actual).toBeTruthy();
	});
});

describe('existNodesEdge', () => {
	it('should pass', () => {
		const actual = existNodesEdge();
		expect(actual).toBeTruthy();
	});
});

export { };