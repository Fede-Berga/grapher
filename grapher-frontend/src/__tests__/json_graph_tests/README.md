# Save graph as a *JSON* (Semi-automatic Test)

In this section is tested the correct generation of the *JSON* file representing the graph drawn on the Whiteboard.


It's considered as a semi-automatic test because the graph has been drawn and downloaded manually from grapher because of incompatibilites with automatic GUI test tools.

Here an image of the graph tested:
![graph](./imgs/json_test.png)
And follows the *JSON* schema representation of a graph:

```json
{
	"$schema": "http://json-schema.org/draft-07/schema#",
	"title": "Sigma Grpah JSON Schema",
	"type": "object",
	"properties": {
		"options": {
			"type": "object",
			"properties": {
				"type": {
					"type": "string"
				},
				"multi": {
					"type": "boolean"
				},
				"allowSelfLoops": {
					"type": "boolean"
				}
			},
			"required": [
				"type",
				"multi",
				"allowSelfLoops"
			]
		},
		"attributes": {
			"type": "object",
			"properties": {},
			"required": []
		},
		"nodes": {
			"type": "array",
			"items": {
				"type": "object",
				"properties": {
					"key": {
						"type": "string"
					},
					"attributes": {
						"type": "object",
						"properties": {
							"x": {
								"type": "number"
							},
							"y": {
								"type": "number"
							},
							"size": {
								"type": "number"
							}
						},
						"required": [
							"x",
							"y",
							"size"
						]
					}
				},
				"required": [
					"key",
					"attributes"
				]
			}
		},
		"edges": {
			"type": "array",
			"items": {
				"type": "object",
				"properties": {
					"key": {
						"type": "string"
					},
					"source": {
						"type": "string"
					},
					"target": {
						"type": "string"
					},
					"attributes": {
						"type": "object",
						"properties": {
							"size": {
								"type": "number"
							}
						},
						"required": [
							"size"
						]
					},
					"undirected": {
						"type": "boolean"
					}
				},
				"required": [
					"key",
					"source",
					"target",
					"attributes",
					"undirected"
				]
			}
		}
	},
	"required": [
		"options",
		"attributes",
		"nodes",
		"edges"
	]
}
```

The following features have been automatically checked:

- The *JSON* schema validation;
- The number of nodes in the *JSON* file is equal to the drawn graph;
- The number of edges in the *JSON* file is equal to the drawn graph;
- The *JSON* file contains exactly 4 fields (options, nodes, edges, attributes);
- The nodes' ID are unique;
- The edges' ID are unique;
- There are not self loops on nodes;
- In the edges' source and target fields, there are actual nodes' IDs.