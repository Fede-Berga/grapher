# Simulation Page / Modal Tests

In this section is tested the correct visualization and interaction of the simulation page.

## Unit tests

These tests are performed in the [unit.test.tsx](./unit.test.tsx) file. They verify the correct rendering of the simulation component itself, as long as the components inside the main one.

## Integration Tests

These tests are performed in the [integration.test.tsx](./integration.test.tsx) file. They verify the correct interacion with the server. Before running the test, make sure that the back-end is up and running.

## End-to-End tests

To perform the **End-to-End tests** make sure the whole application is up and running.

### Simulation button test - Correct Behavior

- Draw a graph in the whiteboard of the Home Page.
- Press the simulation Button
- Select BFS and a starting node of choice
- Press simulate
- The simulation should be displayed

### Simulation button test - No Graph

- When no graph is drown, you should not see the simulation button

### Simulation button test - Unselected Algorithm

- Draw a graph in the whiteboard of the Home Page.
- Press the simulation Button
- Select a starting node of choice
- Press simulate
- An error should be displayed
  
### Simulation button test - Unselected Source

- Draw a graph in the whiteboard of the Home Page.
- Press the simulation Button
- Select an Algorithm
- Press simulate
- An error should be displayed
  
### Simulation panel test -  Correct Behavior

- Execute : **Simulation button test - Correct Behavior**
- Open the explenation panel
- Check that the simulation proceeds as expected
