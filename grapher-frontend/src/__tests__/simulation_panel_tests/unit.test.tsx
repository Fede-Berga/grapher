import SimuLationPanel from '../../components/SimulationPanel'
import Adapter from 'enzyme-adapter-react-16'
import { shallow, configure } from 'enzyme';
import NavbarCustom from '../../components/NavbarCustom';
import HowToUseSimulationPanel from '../../components/HowToUseSimulationPanel';
import { Offcanvas } from 'react-bootstrap';

const sim = {
        'simulation': {
                "0": [
                        "e443ee3219a1414b98ff3223e87bc0b9"
                ],
                "1": [
                        "25baaecc0e7e4b4d9e48124e2ae1aaf6"
                ]
        },
        'status': 200,
};

jest.mock("react-router-dom", () => ({
        ...jest.requireActual("react-router-dom"),
        useLocation: () => ({
                state: sim,
        })
}));

configure({ adapter: new Adapter() });

describe('Simulation Page rendering', () => {

        it('Should correctly render the simulation page', () => {
                const wrapper = shallow(<SimuLationPanel />)
        });

        it('Should correctly render the Navbar on the simulation page', () => {
                const wrapper = shallow(<SimuLationPanel />)
                expect(wrapper.find({ NavbarCustom})).toHaveLength(0);
        });

        it('Should correctly render the how to use button on the Navbar on the simulation page', () => {
                const wrapper = shallow(<SimuLationPanel />);
                expect(wrapper.find(HowToUseSimulationPanel)).toHaveLength(0);
        });

        it('Should correctly render the OFFCANVAS element at the bottom of the simulation page', () => {
                const wrapper = shallow(<SimuLationPanel />);
                expect(wrapper.find(Offcanvas)).toHaveLength(1);
        });
});