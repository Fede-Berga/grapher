import axios from "axios"

const BASE_URL = 'http://127.0.0.1:5000/simulation';

const params = {
    "graph": {
        "options": {
            "type": "mixed",
            "multi": false,
            "allowSelfLoops": true
        },
        "attributes": {},
        "nodes": [
            {
                "key": "c22c54d7-4ee7-44e3-b0f1-9443a66891ad",
                "attributes": {
                    "x": 0.20928898683215108,
                    "y": 0.7855504651438013,
                    "size": 12
                }
            },
            {
                "key": "f88d66ec-db67-4892-9246-642a2ab19408",
                "attributes": {
                    "x": 0.20011467477951678,
                    "y": 0.44380733818455315,
                    "size": 12
                }
            },
            {
                "key": "975f72ca-b0c7-4200-94a6-479825e9641e",
                "attributes": {
                    "x": 0.610665139134901,
                    "y": 0.43807339310134435,
                    "size": 12
                }
            }
        ],
        "edges": [
            {
                "key": "geid_250_0",
                "source": "c22c54d7-4ee7-44e3-b0f1-9443a66891ad",
                "target": "f88d66ec-db67-4892-9246-642a2ab19408",
                "attributes": {
                    "size": 7
                },
                "undirected": true
            },
            {
                "key": "geid_250_1",
                "source": "f88d66ec-db67-4892-9246-642a2ab19408",
                "target": "975f72ca-b0c7-4200-94a6-479825e9641e",
                "attributes": {
                    "size": 7
                },
                "undirected": true
            }
        ]
    },
    "source": "f88d66ec-db67-4892-9246-642a2ab19408"
}

describe('Integration tests for simulation backend', () => {

    it("Should get algos", () => {
        axios.get("/algorithms", {
            baseURL: BASE_URL,
        })
            .then((response) => {
                expect(response.data).toBeValid();
            })
            .catch((err) => {
                expect(err).toBeValid()
            });
    });

    it("should get BFS simulation", () => {
        axios.post(BASE_URL + "/algorithms/bfs", JSON.stringify(params), {
            headers : {
                "Content-Type": "application/json"
            }
        })
        .then((response) => {
            expect(response.data).toBeValid();
        })
        .catch((err) => {
            expect(err).not.toBeNull();
        });
    });
});