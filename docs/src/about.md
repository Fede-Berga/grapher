---
layout: default
nav_order: 6
title: About
permalink: /about
---

# About

Grapher is a project from the *Laboratorio di Progettazione* master degree course.

Team components:

- Albarino Emanuele
- Avenoso Luigi
- Bergamini Federico
- De Iaco Federico
  