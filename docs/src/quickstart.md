---
layout: default
nav_order: 2
title: Quickstart
permalink: /quickstart
---

# Installation guide

To install the Grapher app it is necessary to install [Docker](https://docs.docker.com/desktop/) on your machine.

## Follow these steps to install the Grapher web-app on your **local** machine

1. Clone the gitlab repository <br>
```sh
git clone https://gitlab.com/Fede-Berga/grapher.git
```
2. Enter the grapher folder <br>
```sh 
cd grapher/
```
3. Start the whole app <br>
```sh 
docker compose up -d
```
4. Finally head to **localhost:3000/** and enjoy! 😃