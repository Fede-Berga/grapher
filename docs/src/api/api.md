---
layout: default
title: API
nav_order: 4
permalink: /api
has_children: true
---

# API

This section aims to document the API of the Grapher Backend.

The Grapher Backend can be used to provide the Grapher functionalities to your awesome app!

## Usage (Local)

On your **local machine**, run the backend services with:

```sh 
docker compose up -d
```
Head to one of the **endpoints** to get the desired data, for example:

```sh 
curl localhost:5002/simulation/algorithms'
```
