---
layout: default
nav_order: 5
title: Testing
permalink: /testing
---

# Testing the application

Several testing scenarios have been thought for the application.

In the following page you will find a short description of the kind of tests performed together with how to run them and a link pointing to the relative folders in the repo to get further details.

[Go to the test folder!](https://gitlab.com/Fede-Berga/grapher/-/tree/main/grapher-frontend/src/__tests__){: .btn .btn-outline }

## Front-end integration/Unit Tests

If you are interested in testing the UI of Grapher you can run the following command:

```sh
cd grapher-frontend/
npm run test:ci
```

By running this command you can run the **Unit tests** to check the correct behaviour of the following components and functionalities:

- Navbar rendering ([Go to the folder](https://gitlab.com/Fede-Berga/grapher/-/tree/main/grapher-frontend/src/__tests__/navbar_tests))
- Whiteboard rendering ([Go to the folder](https://gitlab.com/Fede-Berga/grapher/-/tree/main/grapher-frontend/src/__tests__/whiteboard_tests))
- Save graph as a *JSON* ([Go to the folder](https://gitlab.com/Fede-Berga/grapher/-/tree/main/grapher-frontend/src/__tests__/json_graph_tests))
- Save graph as a *DOT* ([Go to the folder](https://gitlab.com/Fede-Berga/grapher/-/tree/main/grapher-frontend/src/__tests__/dot_graph_tests))
- Upload graph ([Go to the folder](https://gitlab.com/Fede-Berga/grapher/-/tree/main/grapher-frontend/src/__tests__/upload_file_tests))
- Simulation Panel ([Go to the folder](https://gitlab.com/Fede-Berga/grapher/-/tree/main/grapher-frontend/src/__tests__/simulation_panel_tests))
- Reset whiteboard ([Go to the folder](https://gitlab.com/Fede-Berga/grapher/-/tree/main/grapher-frontend/src/__tests__/whiteboard_tests))

## Front-end E2E tests

By running these commands you can run **E2E** tests to check the correct UI interaction of the following component:

- Simulation Service ([Go to the folder](https://gitlab.com/Fede-Berga/grapher/-/tree/main/grapher-frontend/src/__tests__/navbar_tests))
- Simulation Panel interaction([Go to the folder](https://gitlab.com/Fede-Berga/grapher/-/tree/main/grapher-backend/simulation/tests))

```sh
    npm i -g selenium-side-runner
    selenium-side-runner ./path/to/file.side
```
{: .note }
Use *sudo* if you are using a Linux distro before *npm*.

- Upload/restore graph to/from cloud ([Go to the folder](https://gitlab.com/Fede-Berga/grapher/-/tree/main/grapher-frontend/src/__tests__/cloud_test))
- Clean whiteboard ([Go to the folder](https://gitlab.com/Fede-Berga/grapher/-/tree/main/grapher-frontend/src/__tests__/whiteboard_tests))


## Back-end Unit tests

By running these commands you can run the **back-end** unit tests. Those tests verify the correctness of the API offered by the App.

- Navbar interaction ([Go to the folder](https://gitlab.com/Fede-Berga/grapher/-/tree/main/grapher-frontend/src/__tests__/navbar_tests))
- Simulation Panel interaction([Go to the folder](https://gitlab.com/Fede-Berga/grapher/-/tree/main/grapher-frontend/src/__tests__/simulation_panel_tests))

```sh
    pytest tests/folder
```
{: .note }
Use *sudo* if you are using a Linux distro before *npm*.

- Upload/restore graph to/from cloud ([Go to the folder](https://gitlab.com/Fede-Berga/grapher/-/tree/main/grapher-backend/dbGraphs/test))

