---
layout: default
title: Home Page
nav_order: 1
permalink: /
---

<div align="center">

<img src="{{site.baseurl | prepend: site.url}}/src/img/logo.png" alt="Lit-LLaMA" width="128"/>

<h1>Grapher</h1>

</div>

Grapher is an awesome tool for creating your own graph and testing some algoritms on it!

## High Level Architecture

The Grapher architecture is a three tier one:

<div align="center">
    <img src="{{site.baseurl | prepend: site.url}}/src/img/high_lvl_arch.png" alt="Lit-LLaMA" width="70%"/>
</div>

- **Web Client**
    - Interface to enjoy the service.
    - Allows a user to draw, import and export graphs.
    - Visualizes the simulation of algorithms.
- **Web Server**
    - Exposes an *API* made up of two services:
        - Simulation : Allows the user to perform algorithms on the provided graph.
        - Graph : allows the user to perform **CRUD** operations on graphs.
- **Database**
    - Memorizes users, graphs and simulations.