---
layout: default
title: How To Use
nav_order: 3
permalink: /howtouse
---

# Welcome to Grapher!

This web-app allows you to draw a simple undirected graph on a Whiteboard.<br>
Here a list of things that you can do:
- **Nodes interactions:**
  
    - Create new nodes by clicking somewhere on the Whiteboard;
    - Move the created nodes as you like;
    - Double click on a node to delete it.

<img src="{{site.baseurl | prepend: site.url}}/src/gifs/node_interact.gif" alt="node_interact.gif" />

- **Edges interactions:**
    - Click on a node and then to another (different) one to create an edge between them;
    - Double click on an edge to delete it.

![Alt Text]({{site.baseurl | prepend: site.url}}/src/gifs/edge_interact.gif)

- **Download your graph:**
    - After you draw your graph click on the download icon at the top right of the screen;
    - Choose the file format (**.json** or **.dot**) from the drop down menu;
    - Click on 'Download', now you have your graph as a file!
![Alt Text]({{site.baseurl | prepend: site.url}}/src/gifs/download_file.gif)

- **Upload your graphs:**
    - Click on the upload icon at the top right of the screen when the whiteboard is empty;
    - Click on 'Choose a file' or drag & drop it inside the box;
    - Click on 'Upload' and you can see the graph on the whiteboard! You can also modify it as you prefer;
    - Just two types of file are supported: **.json** and **.dot**;
    - The **.json** file must have the same structure of the **.json** file downloaded form Grapher;
    - Just undirected graph are supported.
![Alt Text]({{site.baseurl | prepend: site.url}}/src/gifs/upload_file.gif)

- **Upload/restore your graphs to/from cloud:**
    - Click on the upload to cloud icon at the top right of the screen;
    - Then if the upload operation has been successful, save the showed URL;
    - Copy the URL on your browser in order to restore your graph;  
![Alt Text]({{site.baseurl | prepend: site.url}}/src/gifs/upload_cloud.gif)

See [installation guide section](/grapher/quickstart) to start drawing your own graph!
- **Clean the whiteboard:**
    - Click on the trash bin icon at the top left of the whiteboard when you need to erase the graph;
    - Confirm the choice by clicking 'yes' into the banner;
    - Done! Now you can start drawing again.
![Alt Text]({{site.baseurl | prepend: site.url}}/src/gifs/clean_whiteboard.gif)


See [installation guide section](/grapher/quickstart) to start drawing your own graph!
